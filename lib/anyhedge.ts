/* Import anyhedge related interfaces. */
// NOTE: These are imported as objects because we need to use SettlementType enum as an object.
import { ContractMetadata, ContractParameters, ContractFee, SimulationOutput, ContractData, ContractFunding, ContractVersion, UnsignedTransactionProposal, SignedTransactionProposal, ParsedSettlementData, SettlementType, AnyHedgeManagerConfig, ContractRegistrationParameters, ContractCreationParameters, ContractValidationParameters, ContractSettlementParameters, AutomatedPayoutParameters, UnsignedFundingProposal, SignedFundingProposal, ContractAutomatedPayout, ContractMaturation, ContractLiquidation } from './interfaces/index.js';

import { binToHex, encodeTransaction, hexToBin, secp256k1, utf8ToBin, sha256 } from '@bitauth/libauth';
import { OracleData } from '@generalprotocols/price-oracle';
// eslint-disable-next-line import/no-internal-modules
import { Contract, ElectrumNetworkProvider, NetworkProvider, Transaction } from 'cashscript';
import fetch from 'node-fetch';
import { DUST_LIMIT, SATS_PER_BCH, DEFAULT_SERVICE_DOMAIN, DEFAULT_SERVICE_PORT, DEFAULT_SERVICE_SCHEME, DEFAULT_CONTRACT_VERSION } from './constants.js';
import { debug } from './util/javascript-util.js';
import { contractCoinToFunding, contractFundingToCoin, buildPayoutTransaction, estimatePayoutTransactionFee, parseSettlementTransaction, contractFundingToOutpoint } from './util/anyhedge-util.js';
import { addressToLockScript, broadcastTransaction, decodeWIF, derivePublicKey, encodeCashAddressP2PKH, lockScriptToAddress } from './util/bitcoincash-util.js';
import { extractRedemptionDataList, attemptTransactionGeneration, mergeSignedProposals, unlockingDataFromWIF, unlockingDataFromRedemptionData } from './util/mutual-redemption-util.js';
import { AnyHedgeArtifacts } from '@generalprotocols/anyhedge-contracts';
import { MissingAuthenticationTokenError } from './errors.js';
import { calculateRequiredFundingSatoshis, decodeUnlockingBytecodeP2PKH, generateTransactionFromFundingProposal, mergeTransactionInputs, signTransactionP2PKH } from './util/funding-util.js';
import { validateHighLiquidationPrice, validateLowLiquidationPrice, validateNominalUnitsXSatsPerBch, validatePayoutSats } from './util/constraints-validation-util.js';
import equals from 'shallow-equals';

/**
 * Class that manages AnyHedge contract operations, such as creation, validation, and payout of AnyHedge contracts.
 */
// Disable ESLint prefer-default-export rule because 'export default' does not play well with commonjs
// eslint-disable-next-line import/prefer-default-export
export class AnyHedgeManager
{
	contractVersion: ContractVersion;
	serviceScheme: string;
	serviceDomain: string;
	servicePort: number;
	networkProvider: NetworkProvider;
	authenticationToken?: string;

	get serviceUrl(): string
	{
		return `${this.serviceScheme}://${this.serviceDomain}:${this.servicePort}`;
	}

	/**
	 * Initializes an AnyHedge Manager using the specified config options. Note that the `networkProvider` and `electrumCluster`
	 * options are mutually exclusive and the `networkProvider` takes precedence. The default network provider automatically
	 * connects and disconnects between network requests, so if you need a persistent connection, please use a custom provider.
	 *
	 * @param {AnyHedgeManagerConfig} config             config object containing configuration options for the AnyHedge Manager.
	 *
	 * @see {@link https://gitlab.com/GeneralProtocols/anyhedge/contracts|AnyHedge contracts repository} for a list of contract versions.
	 *
	 * @example const anyHedgeManager = new AnyHedgeManager({ authenticationToken: '<token>' });
	 * @example
	 * const config =
	 * {
	 * 	authenticationToken: '<token>',
	 * 	serviceDomain: 'localhost',
	 * 	servicePort: 6572,
	 * 	serviceScheme: 'http',
	 * 	networkProvider: new ElectrumNetworkProvider('mainnet')
	 * };
	 * const anyHedgeManager = new AnyHedgeManager(config);
	 */
	constructor(config: AnyHedgeManagerConfig = {})
	{
		// Extract service URL components from the config object
		this.serviceScheme = config.serviceScheme || DEFAULT_SERVICE_SCHEME;
		this.serviceDomain = config.serviceDomain || DEFAULT_SERVICE_DOMAIN;
		this.servicePort = config.servicePort || DEFAULT_SERVICE_PORT;

		// Store the contract version
		this.contractVersion = config.contractVersion || DEFAULT_CONTRACT_VERSION;

		// Store the authentication token
		this.authenticationToken = config.authenticationToken;

		if(config.networkProvider)
		{
			// Use the provided network provider for BCH network operations if one is provided.
			this.networkProvider = config.networkProvider;
		}
		else
		{
			// If a custom cluster is provided, connection management is handled by the user.
			const manualConnectionManagement = config.electrumCluster !== undefined;

			// Create a new ElectrumNetworkProvider for BCH network operations, optionally using the provided electrum cluster.
			this.networkProvider = new ElectrumNetworkProvider('mainnet', config.electrumCluster, manualConnectionManagement);
		}
	}

	/*
	// External library functions
	*/

	/**
	 * Request an authentication token from the settlement service. This token
	 * is used to authenticate all requests to the settlement services. Only a single
	 * token needs to be generated per consuming application.
	 *
	 * @param name   name to communicate an identity to the settlement service.
	 *
	 * @throws {Error} if the request failed.
	 * @returns a new authentication token
	 */
	async requestAuthenticationToken(name: string): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'requestAuthenticationToken() <=', arguments ]);

		// Request a new authentication token from the settlement service
		const requestParameters =
		{
			method: 'post',
			body: JSON.stringify({ name }),
			headers: { 'Content-Type': 'application/json' },
		};
		const requestResponse = await fetch(`${this.serviceUrl}/api/v1/requestToken`, requestParameters);

		// Throw the returned error if the request failed
		if(!requestResponse.ok)
		{
			throw(new Error(await requestResponse.text()));
		}

		// Retrieve the authentication from the response if the request succeeded
		const authenticationToken = await requestResponse.text();

		// Output function result for easier collection of test data.
		debug.result([ 'requestAuthenticationToken() =>', authenticationToken ]);

		return authenticationToken;
	}

	/**
	 * Register a new contract for external management.
	 *
	 * @param contractAddress            contract address to submit funding for.
	 * @param transactionHex             funding transaction as a hex-encoded string.
	 * @param [dependencyTransactions]   list of transaction hex strings of transactions that the funding transaction depends on.
	 *
	 * @throws {Error} if no authentication token is provided or the authentication token is invalid.
	 * @throws {Error} if the API call failed in any way (e.g. the transaction failed to broadcast).
	 * @returns funding information for the registered contract.
	 */
	async submitFundingTransaction(
		contractAddress: string,
		transactionHex: string,
		dependencyTransactions?: string[],
	): Promise<ContractFunding>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'submitFundingTransaction() <=', arguments ]);

		// Check that an authentication token is set
		if(!this.authenticationToken)
		{
			throw(new MissingAuthenticationTokenError());
		}

		// Submit the funding transaction to the automatic settlement service provider.
		const requestParameters =
		{
			method: 'post',
			body: JSON.stringify({ contractAddress, transactionHex, dependencyTransactions }),
			headers: { 'Content-Type': 'application/json', Authorization: this.authenticationToken },
		};
		const requestResponse = await fetch(`${this.serviceUrl}/api/v1/submitFunding`, requestParameters);

		// If the API call was not successful..
		if(!requestResponse.ok)
		{
			// .. pass through the error to the developers.
			throw(new Error(await requestResponse.text()));
		}

		// If no error is returned, the response can be parsed as JSON.
		const contractFunding = await requestResponse.json() as ContractFunding;

		// Output function result for easier collection of test data.
		debug.result([ 'submitFundingTransaction() =>', contractFunding ]);

		// Return the funding information.
		return contractFunding;
	}

	/**
	 * Register a new contract for external management.
	 *
	 * @param contractRegistrationParameters   object containing the parameters to register this contract.
	 *
	 * @throws {Error} if no authentication token is provided or the authentication token is invalid.
	 * @returns contract information for the registered contract.
	 */
	async registerContractForSettlement(contractRegistrationParameters: ContractRegistrationParameters, optionalServiceFees: Array<ContractFee> = []): Promise<ContractData>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'registerContractForSettlement() <=', arguments ]);

		// Check that an authentication token is set
		if(!this.authenticationToken)
		{
			throw(new MissingAuthenticationTokenError());
		}

		// Register the contract with the automatic settlement service provider.
		const requestParameters =
		{
			method: 'post',
			body: JSON.stringify({ ...contractRegistrationParameters, fees: optionalServiceFees }),
			headers: { 'Content-Type': 'application/json', Authorization: this.authenticationToken },
		};
		const contractResponse = await fetch(`${this.serviceUrl}/api/v1/registerContract`, requestParameters);

		// If the API call was not successful..
		if(!contractResponse.ok)
		{
			// .. pass through the error to the developers.
			throw(new Error(await contractResponse.text()));
		}

		// If no error is returned, the response can be parsed as JSON.
		const contractData = await contractResponse.json() as ContractData;

		// Write log entry for easier debugging.
		debug.action(`Registered contract '${contractData.address}' with ${this.serviceDomain} for automatic settlement.`);

		// Validate that the contract returned is identical to a contract created locally.
		const contractValidity = await this.validateContract({ ...contractRegistrationParameters, contractAddress: contractData.address });

		// If the contract is invalid..
		if(!contractValidity)
		{
			// Write a log entry explaining the problem and throw the error.
			const errorMsg = `Contract registration for '${contractData.address}' with ${this.serviceDomain} resulted in an invalid contract.`;
			debug.errors(errorMsg);

			throw(new Error(errorMsg));
		}

		// Output function result for easier collection of test data.
		debug.result([ 'registerContractForSettlement() =>', contractData ]);

		// Return the contract information.
		return contractData;
	}

	/**
	 * Request the contract data and status of a contract with the settlement service.
	 *
	 * @param contractAddress address to retrieve status for
	 * @param [privateKeyWIF] private key WIF of one of the contract's parties.
	 *
	 * @throws {Error} if no authentication token is provided or the authentication token is invalid.
	 * @throws {Error} if no private key WIF was provided *and* the authentication token is different than the one used for registration.
	 * @throws {Error} if an invalid WIF was provided.
	 * @throws {Error} if a private key WIF was provided that does not belong to either of the contract parties.
	 * @throws {Error} if no contract is registered at the settlement service for the given address.
	 * @throws {Error} if the API call is unsuccessful.
	 * @returns the contract data and status of the contract
	 */
	async getContractStatus(
		contractAddress: string,
		privateKeyWIF?: string,
	): Promise<ContractData>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'getContractStatus() <=', arguments ]);

		// Check that an authentication token is set.
		if(!this.authenticationToken)
		{
			throw(new MissingAuthenticationTokenError());
		}

		// Initialize empty public key and signature.
		let publicKey = '';
		let signature = '';

		// Generate a public key and signature if a private key WIF was provided.
		if(privateKeyWIF)
		{
			// Decode the private key WIF if it was provided.
			const privateKey = await decodeWIF(privateKeyWIF);

			// Derive the corresponding public key.
			publicKey = await derivePublicKey(privateKey);

			// Hash the contract address to be signed.
			const messageHash = await sha256.hash(utf8ToBin(contractAddress));

			// Sign the message hash using the provided private key.
			const signatureBin = secp256k1.signMessageHashSchnorr(hexToBin(privateKey), messageHash);
			if(typeof signatureBin === 'string')
			{
				throw new Error(signatureBin);
			}

			// Convert the signature to a hex string.
			signature = binToHex(signatureBin);
		}

		// Request the contract's status from the settlement service
		const queryParameters = `contractAddress=${contractAddress}&signature=${signature}&publicKey=${publicKey}`;
		const requestParameters =
		{
			method: 'get',
			headers: { Authorization: this.authenticationToken },
		};
		const contractResponse = await fetch(`${this.serviceUrl}/api/v1/contractStatus?${queryParameters}`, requestParameters);

		// If the API call was not successful..
		if(!contractResponse.ok)
		{
			// .. pass through the error to the developers.
			throw(new Error(await contractResponse.text()));
		}

		// If no error is returned, the response can be parsed as JSON.
		const receivedContractData = await contractResponse.json() as ContractData;

		// Verify that the received contract address matches the requested address.
		if(receivedContractData.address !== contractAddress)
		{
			throw(new Error(`Received contract data for contract ${receivedContractData.address} while requesting contract data for contract ${contractAddress}`));
		}

		// Combine the received contracts metadata and parameters to form a valid set of contract creation parameters.
		const creationParameters = { ...receivedContractData.metadata, ...receivedContractData.parameters };

		// Use the received contract data to independently reconstruct the contract data.
		const reconstructedContractData = await this.createContract(creationParameters);

		// Verify that the reconstructed data matches the received data.
		const addressMatches = receivedContractData.address === reconstructedContractData.address;
		const metadataMatches = equals(receivedContractData.metadata, reconstructedContractData.metadata);
		const parametersMatch = equals(receivedContractData.parameters, reconstructedContractData.parameters);

		if(!addressMatches || !metadataMatches || !parametersMatch)
		{
			throw(new Error(`Invalid contract data received for contract ${contractAddress}`));
		}

		debug.action(`Retrieved contract status for '${contractAddress}' from ${this.serviceDomain}.`);

		// Output function result for easier collection of test data.
		debug.result([ 'getContractStatus() =>', receivedContractData ]);

		return receivedContractData;
	}

	/**
	 * Validates that a given contract address matches specific contract parameters.
	 *
	 * @param contractValidationParameters   object containing the parameters to validate this contract.
	 *
	 * @returns true if the contract address and parameters match, otherwise false.
	 */
	async validateContract(contractValidationParameters: ContractValidationParameters): Promise<boolean>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'validateContract() <=', arguments ]);

		// Extract the relevant parameters
		const { contractAddress } = contractValidationParameters;

		// Prepare the contract.
		const contractData = await this.createContract(contractValidationParameters);

		// Build the contract.
		const contract = await this.compileContract(contractData.parameters);

		// Calculate contract validity.
		const contractValidity = (contractAddress === contract.address);

		if(contractValidity)
		{
			// Write log entry for easier debugging.
			debug.action(`Validated a contract address (${contractAddress}) against provided contract parameters.`);
			// eslint-disable-next-line prefer-rest-params
			debug.object(arguments);
		}
		else
		{
			// Write log entry for easier debugging.
			debug.errors(`Failed to validate the provided contract address (${contractAddress}) against provided contract parameters generated address (${contract.address}).`);
			// eslint-disable-next-line prefer-rest-params
			debug.object(arguments);
		}

		// Output function result for easier collection of test data.
		debug.result([ 'validateContract() =>', contractValidity ]);

		// Return the validity of the contract.
		return contractValidity;
	}

	/**
	 * Build and broadcast a custodial mutual redemption transaction with arbitrary transaction details.
	 *
	 * @param hedgePrivateKeyWIF    hedge's private key WIF.
	 * @param longPrivateKeyWIF     long's private key WIF.
	 * @param transactionProposal   unsigned transaction proposal for the mutual redemption.
	 * @param contractParameters    contract parameters of the relevant contract.
	 *
	 * @throws {Error} if any of the private key WIF strings is not valid.
	 * @throws {Error} if any of the private key WIFs does not belong to a party of the contract.
	 * @throws {Error} if the generated transaction could not successfully be broadcasted.
	 * @returns transaction ID of the broadcasted mutual redemption transaction.
	 */
	async custodialMutualArbitraryPayout(
		hedgePrivateKeyWIF: string,
		longPrivateKeyWIF: string,
		transactionProposal: UnsignedTransactionProposal,
		contractParameters: ContractParameters,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'custodialMutualArbitraryPayout() <=', arguments ]);

		// Sign the proposal using both keys.
		const hedgeProposal = await this.signMutualArbitraryPayout(hedgePrivateKeyWIF, transactionProposal, contractParameters);
		const longProposal = await this.signMutualArbitraryPayout(longPrivateKeyWIF, transactionProposal, contractParameters);

		// Build and broadcast the mutual redemption transaction with both signed proposals.
		const transactionID = await this.completeMutualRedemption(hedgeProposal, longProposal, contractParameters);

		// Output function result for easier collection of test data.
		debug.result([ 'custodialMutualArbitraryPayout() =>', transactionID ]);

		return transactionID;
	}

	/**
	 * Build and broadcast a custodial mutual redemption transaction that mimics a
	 * maturation before the actual maturation timestamp.
	 *
	 * @param hedgePrivateKeyWIF   hedge's private key WIF.
	 * @param longPrivateKeyWIF    long's private key WIF.
	 * @param contractFunding      the specific Contract Funding to use in the custodial early maturation.
	 * @param settlementPrice      price to use in settlement.
	 * @param contractParameters   contract parameters of the relevant contract.
	 *
	 * @throws {Error} if any of the private key WIF strings is not valid.
	 * @throws {Error} if any of the private key WIFs does not belong to a party of the contract.
	 * @throws {Error} if the generated transaction could not successfully be broadcasted.
	 * @returns transaction ID of the broadcasted mutual redemption transaction.
	 */
	async custodialMutualEarlyMaturation(
		{
			hedgePrivateKeyWIF,
			longPrivateKeyWIF,
			contractFunding,
			settlementPrice,
			contractParameters,
		}
		:
		{
			hedgePrivateKeyWIF: string;
			longPrivateKeyWIF: string;
			contractFunding: ContractFunding;
			settlementPrice: number;
			contractParameters: ContractParameters;
		},
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'custodialMutualEarlyMaturation() <=', arguments ]);

		// Sign the settlement using both keys.
		const hedgeProposal = await this.signMutualEarlyMaturation(
			{
				privateKeyWIF: hedgePrivateKeyWIF,
				contractFunding,
				settlementPrice,
				contractParameters,
			},
		);
		const longProposal = await this.signMutualEarlyMaturation(
			{
				privateKeyWIF: longPrivateKeyWIF,
				contractFunding,
				settlementPrice,
				contractParameters,
			},
		);

		// Build and broadcast the mutual redemption transaction with both signed proposals.
		const transactionID = await this.completeMutualRedemption(hedgeProposal, longProposal, contractParameters);

		// Output function result for easier collection of test data.
		debug.result([ 'custodialMutualEarlyMaturation() =>', transactionID ]);

		return transactionID;
	}

	/**
	 * Build and broadcast a custodial mutual redemption transaction that refunds
	 * the contract's funds based on the provided contract metadata. Optionally
	 * allows you to provide separate refund addresses. If these are omitted,
	 * the mutual redemption public keys are used to receive the refunds.
	 *
	 * @param hedgePrivateKeyWIF     hedge's private key WIF.
	 * @param longPrivateKeyWIF      long's private key WIF.
	 * @param contractFunding        the specific Contract Funding to use in the custodial refund.
	 * @param contractParameters     contract parameters of the relevant contract.
	 * @param contractMetadata       contract metadata of the relevant contract.
	 * @param [hedgeRefundAddress]   hedge's address to receive the refund.
	 * @param [longRefundAddress]    long's address to receive the refund.
	 *
	 * @throws {Error} if any of the private key WIF strings is not valid.
	 * @throws {Error} if any of the private key WIFs does not belong to a party of the contract.
	 * @throws {Error} if the generated transaction could not successfully be broadcasted.
	 * @returns transaction ID of the broadcasted mutual redemption transaction.
	 */
	async custodialMutualRefund({
		hedgePrivateKeyWIF,
		longPrivateKeyWIF,
		contractFunding,
		contractParameters,
		contractMetadata,
		hedgeRefundAddress,
		longRefundAddress,
	}:
	{
		hedgePrivateKeyWIF: string;
		longPrivateKeyWIF: string;
		contractFunding: ContractFunding;
		contractParameters: ContractParameters;
		contractMetadata: ContractMetadata;
		hedgeRefundAddress?: string;
		longRefundAddress?: string; }): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'custodialMutualRefund() <=', arguments ]);

		// Sign the refund using hedge key.
		const hedgeProposal = await this.signMutualRefund(
			{
				privateKeyWIF: hedgePrivateKeyWIF,
				contractFunding,
				contractParameters,
				contractMetadata,
				hedgeRefundAddress,
				longRefundAddress,
			},
		);

		// Sign the refund using long key.
		const longProposal = await this.signMutualRefund(
			{
				privateKeyWIF: longPrivateKeyWIF,
				contractFunding,
				contractParameters,
				contractMetadata,
				hedgeRefundAddress,
				longRefundAddress,
			},
		);

		// Build and broadcast the mutual redemption transaction with both signed proposals.
		const transactionID = await this.completeMutualRedemption(hedgeProposal, longProposal, contractParameters);

		// Output function result for easier collection of test data.
		debug.result([ 'custodialMutualRefund() =>', transactionID ]);

		return transactionID;
	}

	/**
	 * Sign a mutual redemption transaction that mimics a maturation before the
	 * actual maturation timestamp. Both parties need to call this function with
	 * the same input and settlement price. Both signed transaction proposals must then
	 * be passed into the completeMutualRedemption() function to broadcast the transaction.
	 *
	 * @param privateKeyWIF        private key WIF of one of the contract's parties.
	 * @param contractFunding      the specific Contract Funding to use in the mutual early maturation.
	 * @param settlementPrice      price to use in settlement.
	 * @param contractParameters   contract parameters of the relevant contract.
	 *
	 * @throws {Error} if the private key WIF string is not valid.
	 * @throws {Error} if the private key WIF does not belong to a party of the contract.
	 * @returns a signed settlement transaction proposal.
	 */
	async signMutualEarlyMaturation(
		{
			privateKeyWIF,
			contractFunding,
			settlementPrice,
			contractParameters,
		}:
		{ privateKeyWIF: string;
			contractFunding: ContractFunding;
			settlementPrice: number;
			contractParameters: ContractParameters; },
	): Promise<SignedTransactionProposal>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'signMutualEarlyMaturation() <=', arguments ]);

		// Check that the provided settlement price is within the contract's bounds.
		if(settlementPrice < contractParameters.lowLiquidationPrice || settlementPrice > contractParameters.highLiquidationPrice)
		{
			throw(new Error('Settlement price is out of liquidation bounds, which is unsupported by a mutual early maturation.'));
		}

		// Calculate settlement outcome.
		const outcome = await this.calculateSettlementOutcome(contractParameters, contractFunding.fundingSatoshis, settlementPrice);

		// Derive hedge/long settlement addresses.
		const hedgeAddress = lockScriptToAddress(contractParameters.hedgeLockScript);
		const longAddress = lockScriptToAddress(contractParameters.longLockScript);

		// Build transaction proposal based on these parameters.
		const inputs = [ contractFundingToCoin(contractFunding) ];
		const outputs =
		[
			{ to: hedgeAddress, amount: outcome.hedgePayoutSatsSafe },
			{ to: longAddress, amount: outcome.longPayoutSatsSafe },
		];
		const unsignedProposal = { inputs, outputs };

		// Sign the proposal.
		const signedProposal = await this.signMutualArbitraryPayout(privateKeyWIF, unsignedProposal, contractParameters);

		// Output function result for easier collection of test data.
		debug.result([ 'signMutualEarlyMaturation() =>', signedProposal ]);

		return signedProposal;
	}

	/**
	 * Sign a mutual redemption transaction that refunds the contract's funds based on
	 * the provided contract metadata. Optionally allows you to provide separate
	 * refund addresses. If these are omitted, the mutual redemption public keys
	 * are used to receive the refunds. Both parties need to call this function with
	 * the same contract funding, contract metadata and refund addresses. Both signed
	 * transaction proposals must then be passed into the completeMutualRedemption()
	 * function to broadcast the transaction.
	 *
	 * @param privateKeyWIF          private key WIF of one of the contract's parties.
	 * @param contractFunding        the specific Contract Funding to use in the mutual refund.
	 * @param contractParameters     contract parameters of the relevant contract.
	 * @param contractMetadata       contract metadata of the relevant contract.
	 * @param [hedgeRefundAddress]   hedge's address to receive the refund.
	 * @param [longRefundAddress]    long's address to receive the refund.
	 *
	 * @throws {Error} if the private key WIF string is not valid.
	 * @throws {Error} if the private key WIF does not belong to a party of the contract.
	 * @returns a signed refund transaction proposal.
	 */
	async signMutualRefund(
		{
			privateKeyWIF,
			contractFunding,
			contractParameters,
			contractMetadata,
			hedgeRefundAddress,
			longRefundAddress,
		}:
		{
			privateKeyWIF: string;
			contractFunding: ContractFunding;
			contractParameters: ContractParameters;
			contractMetadata: ContractMetadata;
			hedgeRefundAddress?: string;
			longRefundAddress?: string; },
	): Promise<SignedTransactionProposal>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'signMutualRefund() <=', arguments ]);

		// If no refund addresses are provided, derive them from the private keys
		const hedgeAddress = hedgeRefundAddress || await encodeCashAddressP2PKH(contractParameters.hedgeMutualRedeemPublicKey);
		const longAddress = longRefundAddress || await encodeCashAddressP2PKH(contractParameters.longMutualRedeemPublicKey);

		// To ensure the transaction is valid, we put the minimum output value to DUST
		const hedgeAmount = Math.max(contractMetadata.hedgeInputInSatoshis, DUST_LIMIT);
		const longAmount = Math.max(contractMetadata.longInputInSatoshis, DUST_LIMIT);

		// Build transaction proposal based on the provided parameters.
		const inputs = [ contractFundingToCoin(contractFunding) ];
		const outputs =
		[
			{ to: hedgeAddress, amount: hedgeAmount },
			{ to: longAddress, amount: longAmount },
		];
		const unsignedProposal = { inputs, outputs };

		// Sign the proposal.
		const signedProposal = await this.signMutualArbitraryPayout(privateKeyWIF, unsignedProposal, contractParameters);

		// Output function result for easier collection of test data.
		debug.result([ 'signMutualRefund() =>', signedProposal ]);

		return signedProposal;
	}

	/**
	 * Sign a mutual redemption transaction proposal with arbitrary transaction details.
	 * Both parties need to call this function with the same transaction details.
	 * Both signed transaction proposals must then be passed into the
	 * completeMutualRedemption() function to broadcast the transaction.
	 *
	 * @param privateKeyWIF         private key WIF of one of the contract's parties.
	 * @param transactionProposal   An unsigned proposal for a transaction.
	 * @param contractParameters    contract parameters for the relevant contract.
	 *
	 * @throws {Error} if the private key WIF string is not valid.
	 * @throws {Error} if the private key WIF does not belong to a party of the contract.
	 * @throws {Error} if a valid transaction is generated during the preparation.
	 * @returns updated transaction proposal with the resolved variables added in.
	 */
	async signMutualArbitraryPayout(
		privateKeyWIF: string,
		transactionProposal: UnsignedTransactionProposal,
		contractParameters: ContractParameters,
	): Promise<SignedTransactionProposal>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'signMutualArbitraryPayout() <=', arguments ]);

		// Check that mutual redemption is enabled
		if(contractParameters.enableMutualRedemption !== 1)
		{
			throw(new Error('Mutual redemption is disabled for this contract.'));
		}

		// Check that there are no obvious errors in the transaction proposal.

		// Check that there are no outputs below the DUST amount.
		if(transactionProposal.outputs.find((output) => output.amount < DUST_LIMIT))
		{
			throw(new Error(`One of the outputs in the transaction proposal is below the DUST amount of ${DUST_LIMIT}.`));
		}

		// Get the contract's redeem script.
		const contract = await this.compileContract(contractParameters);
		const redeemScriptHex = contract.getRedeemScriptHex();

		// Generate unlocking data from the private key WIF
		const unlockingData = await unlockingDataFromWIF(privateKeyWIF, contractParameters);

		// Create a list of unlocking data for every input (same data for every input).
		const unlockingDataPerInput = transactionProposal.inputs.map(() => unlockingData);

		// Attempt to generate a transaction using the passed parameters.
		const attempt = await attemptTransactionGeneration(transactionProposal, redeemScriptHex, unlockingDataPerInput);

		// Check that the attempt was not successful (should never happen).
		if(attempt.success)
		{
			throw(new Error('Internal Error: should not be able to generate valid mutual redemption without both parties'));
		}

		// Extract the relevant redemption data list from the transaction generation attempt.
		const redemptionDataList = extractRedemptionDataList(attempt);

		// Update the transaction proposal by adding the new redemption data list.
		const signedProposal = { ...transactionProposal, redemptionDataList };

		// Output function result for easier collection of test data.
		debug.result([ 'signMutualArbitraryPayout() =>', signedProposal ]);

		return signedProposal;
	}

	/**
	 * Complete a mutual redemption by generating a valid transaction from both parties'
	 * signed proposals and broadcasting it. Both parties need to generate and sign the same
	 * transaction proposal using signMutualEarlyMaturation(), signMutualRefund() or
	 * signMutualArbitraryPayout().
	 *
	 * @param signedProposal1      transaction proposal signed by one of the two parties.
	 * @param signedProposal2      transaction proposal signed by the other party.
	 * @param contractParameters   contract parameters for the relevant contract.
	 *
	 * @throws {Error} if any proposal is unsigned.
	 * @throws {Error} if the transaction details of both proposals don't match.
	 * @throws {Error} if the redemption data lists of the proposals have different lengths.
	 * @throws {Error} if both proposals are signed by the same party.
	 * @throws {Error} if the generated transaction could not successfully be broadcasted.
	 * @returns transaction ID of the broadcasted mutual redemption transaction.
	 */
	async completeMutualRedemption(
		signedProposal1: SignedTransactionProposal,
		signedProposal2: SignedTransactionProposal,
		contractParameters: ContractParameters,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'completeMutualRedemption() <=', arguments ]);

		// Check that mutual redemption is enabled
		if(contractParameters.enableMutualRedemption !== 1)
		{
			throw(new Error('Mutual redemption is disabled for this contract.'));
		}

		// Check that the transaction proposal includes redemption data
		if(!signedProposal1.redemptionDataList || !signedProposal2.redemptionDataList)
		{
			const errorMsg = 'Transaction proposal does not include any redemption data. '
				+ 'Make sure that both parties signed their transaction proposals using '
				+ 'signMutualRedemption(), signSettlement() or signRefund().';
			throw(new Error(errorMsg));
		}

		// Merge both proposals, combining their redemption data.
		const mergedProposal = mergeSignedProposals(signedProposal1, signedProposal2);

		// Get the contract's redeem script.
		const contract = await this.compileContract(contractParameters);
		const redeemScriptHex = contract.getRedeemScriptHex();

		// Generate unlocking data for all inputs of the transaction using the passed redemption data
		const unlockingDataPerInput = mergedProposal.redemptionDataList.map(unlockingDataFromRedemptionData);

		// Attempt to generate a transaction using the passed parameters.
		const attempt = await attemptTransactionGeneration(mergedProposal, redeemScriptHex, unlockingDataPerInput);

		// Check that the transaction generation didn't fail (happens if the proposal was only signed by a single party)
		if(!attempt.success)
		{
			const errorMsg = 'Mutual redemption could not successfully be completed. '
				+ 'Make sure that the passed proposals are signed by different parties.';
			throw(new Error(errorMsg));
		}

		// Hex encode the generated transaction.
		const transactionHex = binToHex(encodeTransaction(attempt.transaction));

		// Broadcast the transaction.
		const broadcastResult = await broadcastTransaction(transactionHex, this.networkProvider);

		// Output function result for easier collection of test data.
		debug.result([ 'completeMutualRedemption() =>', broadcastResult ]);

		return broadcastResult;
	}

	/**
	 * Calculate the total satoshis required as input to a collaborative funding transaction.
	 * This includes the contract input satoshis, contract safety fees, potential settlement
	 * service fees and funding transaction fees. This assumes 2 P2PKH inputs to the funding
	 * transaction.
	 *
	 * @param contractData   contract data for which to calculate funding satoshis
	 *
	 * @returns total required funding satoshis
	 */
	calculateTotalRequiredFundingSatoshis(contractData: ContractData): number
	{
		return calculateRequiredFundingSatoshis(contractData);
	}

	/**
	 * Create an unsigned funding proposal.
	 * @param contractData              contract data for the proposal
	 * @param outpointTransactionHash   outpoint transaction hash of the UTXO for the proposal
	 * @param outpointIndex             outpoint index of the UTXO for the proposal
	 * @param satoshis                  satoshi amount of the UTXO for the proposal
	 * @returns unsigned funding proposal created from the provided data.
	 */
	createFundingProposal(
		contractData: ContractData,
		outpointTransactionHash: string,
		outpointIndex: number,
		satoshis: number,
	): UnsignedFundingProposal
	{
		// Construct an unsigned funding proposal using the provided data
		const proposal =
		{
			contractData,
			utxo:
			{
				txid: outpointTransactionHash,
				vout: outpointIndex,
				satoshis,
			},
		};

		return proposal;
	}

	/**
	 * Sign a partial funding transaction for a contract and add the signature and public key to the proposal.
	 *
	 * @param privateKeyWIF     private key WIF of one of the contract's parties.
	 * @param fundingProposal   An unsigned proposal for a contract funding.
	 *
	 * @throws {Error} if the private key WIF string is not valid.
	 * @throws {Error} if a valid transaction is generated during the preparation.
	 * @returns updated transaction proposal with the resolved variables added in.
	 */
	async signFundingProposal(
		privateKeyWIF: string,
		fundingProposal: UnsignedFundingProposal,
	): Promise<SignedFundingProposal>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'signFundingProposal() <=', arguments ]);

		// Deterministically generate a partial, unsigned transaction from the funding proposal.
		const unsignedTransaction = generateTransactionFromFundingProposal(fundingProposal);

		// Sign the unsigned transaction.
		const signedTransaction = await signTransactionP2PKH(privateKeyWIF, unsignedTransaction);

		// Extract the signature from the signed transaction.
		const decodedUnlockingBytecode = decodeUnlockingBytecodeP2PKH(signedTransaction.inputs[0].unlockingBytecode);

		// Construct a signed funding proposal object.
		const signedFundingProposal = { ...fundingProposal, ...decodedUnlockingBytecode };

		// Output function result for easier collection of test data.
		debug.result([ 'signFundingProposal() =>', signedFundingProposal ]);

		return signedFundingProposal;
	}

	/**
	 * Complete a funding by combining two signed funding proposals and returning the resulting transaction hex.
	 *
	 * @param signedProposal1   first signed proposal.
	 * @param signedProposal2   second signed proposal.
	 *
	 * @throws {Error} if total provided satoshis is not exactly the total required satoshis.
	 * @throws {Error} if the two funding proposals belong to different contracts.
	 * @returns transaction ID for the broadcasted funding transaction.
	 */
	async completeFundingProposal(
		signedProposal1: SignedFundingProposal,
		signedProposal2: SignedFundingProposal,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'completeFundingProposal() <=', arguments ]);

		// Check that the two proposals are for the same contract by comparing the contractData objects by value
		if(JSON.stringify(signedProposal1.contractData) !== JSON.stringify(signedProposal2.contractData))
		{
			throw(new Error(`Funding proposals do not use the same contract data (1: ${signedProposal1.contractData.address}, 2: ${signedProposal2.contractData.address})`));
		}

		// Calculate the required and provided satoshis (including transaction fees for the funding transaction)
		const totalRequiredSatoshis = calculateRequiredFundingSatoshis(signedProposal1.contractData);
		const totalProvidedSatoshis = signedProposal1.utxo.satoshis + signedProposal2.utxo.satoshis;

		// Check that the provided satoshis exactly the required satoshis
		if(totalProvidedSatoshis !== totalRequiredSatoshis)
		{
			throw(new Error(`Total provided satoshis (${totalProvidedSatoshis}) should be EXACTLY the required satoshis (${totalRequiredSatoshis})`));
		}

		// Generate two partial transactions
		const partialTransaction1 = generateTransactionFromFundingProposal(signedProposal1);
		const partialTransaction2 = generateTransactionFromFundingProposal(signedProposal2);

		// Merge the inputs of transaction 2 into transaction 1
		// Note: we know that the rest of transaction 1 & 2 match because we checked that the contract data matched
		const transaction = mergeTransactionInputs(partialTransaction1, partialTransaction2);

		// Hex encode the generated transaction.
		const transactionHex = binToHex(encodeTransaction(transaction));

		// Output function result for easier collection of test data.
		debug.result([ 'completeFundingProposal() =>', transactionHex ]);

		return transactionHex;
	}

	/**
	 * Liquidates a contract.
	 *
	 * @param contractLiquidationParameters   object containing the parameters to liquidate this contract.
	 *
	 * @returns ContractSettlement object containing the details of the liquidation.
	 */
	async liquidateContractFunding(contractLiquidationParameters: ContractSettlementParameters): Promise<ContractLiquidation>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'liquidateContractFunding() <=', arguments ]);

		// Extract relevant parameters.
		const { contractFunding, settlementMessage, contractParameters } = contractLiquidationParameters;

		// Write log entry for easier debugging.
		debug.action(`Attempting to liquidate contract funding '${contractFundingToOutpoint(contractFunding)}'.`);

		// Parse the settlement message.
		const { messageTimestamp, priceValue } = await OracleData.parsePriceMessage(hexToBin(settlementMessage));

		// Validate that the settlement message timestamp is not at or after the maturation timestamp.
		if(messageTimestamp >= contractParameters.maturityTimestamp)
		{
			// Define an error message
			const errorMessage = `Cannot liquidate contract funding '${contractFundingToOutpoint(contractFunding)}' at or after its maturation timestamp.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Validate that the settlement price is strictly outside liquidation boundaries.
		if(priceValue > contractParameters.lowLiquidationPrice && priceValue < contractParameters.highLiquidationPrice)
		{
			// Define an error message
			const errorMessage = `Cannot liquidate contract funding '${contractFundingToOutpoint(contractFunding)}' at a price within the contract boundaries.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Settle the contract funding.
		const settlementData = await this.settleContractFunding(contractLiquidationParameters) as ContractLiquidation;

		// Output function result for easier collection of test data.
		debug.result([ 'liquidateContractFunding() =>', settlementData ]);

		// Return the liquidation result.
		return settlementData;
	}

	/**
	 * Matures a contract.
	 *
	 * @param contractMaturationParameters   object containing the parameters to mature this contract.
	 *
	 * @returns ContractSettlement object containing the details of the maturation.
	 */
	async matureContractFunding(contractMaturationParameters: ContractSettlementParameters): Promise<ContractMaturation>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'matureContractFunding() <=', arguments ]);

		// Extract relevant parameters.
		const { contractFunding, settlementMessage, contractParameters } = contractMaturationParameters;

		// Write log entry for easier debugging.
		debug.action(`Attempting to mature contract funding '${contractFundingToOutpoint(contractFunding)}'.`);

		// Parse the settlement message.
		const { messageTimestamp } = await OracleData.parsePriceMessage(hexToBin(settlementMessage));

		// Validate that the settlement message's timestamp is not before the contract's maturity timestamp.
		if(messageTimestamp < contractParameters.maturityTimestamp)
		{
			// Define an error message
			const errorMessage = `Cannot mature contract funding '${contractFundingToOutpoint(contractFunding)}' before its maturity height.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Settle the contract funding.
		const settlementData = await this.settleContractFunding(contractMaturationParameters) as ContractMaturation;

		// Output function result for easier collection of test data.
		debug.result([ 'matureContractFunding() =>', settlementData ]);

		// Return the liquidation result.
		return settlementData;
	}

	/**
	 * Settles a contract (this includes both maturation and liquidation).
	 *
	 * @param contractSettlementParameters   object containing the parameters to settle this contract.
	 *
	 * @returns ContractSettlement object containing the details of the settlement.
	 */
	async settleContractFunding(contractSettlementParameters: ContractSettlementParameters): Promise<ContractAutomatedPayout>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'settleContractFunding() <=', arguments ]);

		// Extract relevant parameters.
		const {
			oraclePublicKey,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			contractFunding,
			contractParameters,
		} = contractSettlementParameters;

		// Write log entry for easier debugging.
		debug.action(`Attempting to settle contract funding '${contractFundingToOutpoint(contractFunding)}'.`);

		// Parse the settlement and previous messages.
		const settlementMessageData = await OracleData.parsePriceMessage(hexToBin(settlementMessage));
		const previousMessageData = await OracleData.parsePriceMessage(hexToBin(previousMessage));

		// Validate that the settlement message timestamp is not before contract's start timestamp.
		if(settlementMessageData.messageTimestamp < contractParameters.startTimestamp)
		{
			// Define an error message
			const errorMessage = `Cannot settle contract funding '${contractFundingToOutpoint(contractFunding)}' before its start timestamp.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Validate that the settlement message is immediately after the previous message
		if(settlementMessageData.priceSequence - 1 !== previousMessageData.priceSequence)
		{
			// Define an error message
			const errorMessage = `Cannot settle contract funding '${contractFundingToOutpoint(contractFunding)}' when the provided settlement message does not immediately follow the provided previous message.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Validate that the previous settlement message timestamp is not at or after the contract's maturity timestamp.
		if(previousMessageData.messageTimestamp >= contractParameters.maturityTimestamp)
		{
			// Define an error message
			const errorMessage = `Cannot settle contract funding '${contractFundingToOutpoint(contractFunding)}' with a previous message that is at or after its maturity timestamp.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Validate that the settlement price is not zero or negative.
		if(settlementMessageData.priceValue <= 0)
		{
			// Define an error message
			const errorMessage = `Cannot settle contract funding '${contractFundingToOutpoint(contractFunding)}' at a price of <= 0.`;

			// Log the error message.
			debug.errors(errorMessage);

			// Throw the error.
			throw(new Error(errorMessage));
		}

		// Calculate contract outcomes.
		const outcome = await this.calculateSettlementOutcome(contractParameters, contractFunding.fundingSatoshis, settlementMessageData.priceValue);
		const { hedgePayoutSatsSafe, longPayoutSatsSafe, minerFeeSats } = outcome;

		// Redeem the contract.
		const settlementTransactionHash = await this.automatedPayout({
			oraclePublicKey,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			hedgePayoutSats: hedgePayoutSatsSafe,
			longPayoutSats: longPayoutSatsSafe,
			contractFunding,
			contractParameters,
			minerFeeSats,
		});

		// Determine whether this is a maturation or a liquidation.
		const isMaturation = settlementMessageData.messageTimestamp >= contractParameters.maturityTimestamp;
		const settlementType = isMaturation ? SettlementType.MATURATION : SettlementType.LIQUIDATION;

		// Assemble a ContractSettlement object representing the settlement.
		const settlementData: ContractAutomatedPayout =
		{
			settlementType: settlementType,
			settlementTransactionHash: settlementTransactionHash,
			hedgePayoutInSatoshis: hedgePayoutSatsSafe,
			longPayoutInSatoshis: longPayoutSatsSafe,
			settlementMessage: settlementMessage,
			settlementSignature: settlementSignature,
			previousMessage: previousMessage,
			previousSignature: previousSignature,
			settlementPrice: settlementMessageData.priceValue,
		};

		// Output function result for easier collection of test data.
		debug.result([ 'settleContractFunding() =>', settlementData ]);

		// Return the settlement result.
		return settlementData;
	}

	/**
	 * Redeems the contract with arbitrary numbers.
	 *
	 * @param automatedPayoutParameters   object containing the parameters to payout this contract.
	 *
	 * @returns the transaction ID of a successful redemption.
	 *
	 * @private
	 */
	async automatedPayout(automatedPayoutParameters: AutomatedPayoutParameters): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'automatedPayout() <=', arguments ]);

		// Extract relevant parameters.
		const {
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			hedgePayoutSats,
			longPayoutSats,
			contractFunding,
			contractParameters,
			minerFeeSats,
		} = automatedPayoutParameters;

		// Write log entry for easier debugging.
		debug.action(`Attempting to payout contract funding '${contractFundingToOutpoint(contractFunding)}'.`);

		try
		{
			// Build the contract instance.
			const contract = await this.compileContract(contractParameters);

			// Build the payout transaction.
			const payoutTransaction = await buildPayoutTransaction({
				contract,
				contractParameters,
				contractFunding,
				settlementMessage,
				settlementSignature,
				previousMessage,
				previousSignature,
				hedgePayoutSats,
				longPayoutSats,
				minerFeeSats,
			});

			// Broadcast the transaction.
			const broadcastResult = await this.broadcastTransaction(payoutTransaction);

			// Output function result for easier collection of test data.
			debug.result([ 'automatedPayout() =>', broadcastResult ]);

			// Return the broadcast result.
			return broadcastResult;
		}
		catch(error: any)
		{
			// Define a base error message.
			const baseErrorMessage = `Failed to payout contract funding '${contractFundingToOutpoint(contractFunding)}'`;

			// Log an error message.
			debug.errors(`${baseErrorMessage}: `, error);

			// If the error includes a meep command, we remove it before passing it on.
			const errorMessageExcludingMeep = error.message ? error.message.split('\nmeep')[0] : error;

			// Throw the error.
			throw(new Error(`${baseErrorMessage}: ${errorMessageExcludingMeep}`));
		}
	}

	/*
	// Internal library functions
	*/

	/**
	 * Wrapper that broadcasts a prepared transaction using the CashScript SDK.
	 *
	 * @param transactionBuilder   fully prepared transaction builder ready to execute its broadcast() function.
	 *
	 * @returns the transaction ID of a successful transaction.
	 *
	 * @private
	 */
	async broadcastTransaction(transactionBuilder: Transaction): Promise<string>
	{
		try
		{
			// Broadcast the raw transaction
			const { txid } = await transactionBuilder.send();

			return txid;
		}
		catch(error)
		{
			// Log an error message.
			debug.errors('Failed to broadcast transaction: ', error);

			// Build and log raw transaction hex
			const rawTransactionHex = await transactionBuilder.build();
			debug.errors(rawTransactionHex);

			// Throw the error.
			throw(error);
		}
	}

	/**
	 * Retrieve a list of all ContractFunding instances for a contract.
	 *
	 * @param contractParameters   Contract parameters for the relevant contract.
	 *
	 * @returns list of contract fundings for a contract.
	 */
	async getContractFundings(contractParameters: ContractParameters): Promise<ContractFunding[]>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'getContractFundings() <=', arguments ]);

		// Build the contract.
		const contract = await this.compileContract(contractParameters);

		// Retrieve contract's coins as CashScript UTXOs.
		const coins = await contract.getUtxos();

		// Format the CashScript UTXOs as ContractFunding interfaces.
		const fundings = coins.map(contractCoinToFunding);

		// Output function result for easier collection of test data.
		debug.result([ 'getContractFundings() =>', fundings ]);

		return fundings;
	}

	/**
	 * Simulates contract settlement outcome based on contract parameters, total satoshis in the contract and the redemption price.
	 *
	 * @param contractParameters   contract parameters including price boundaries.
	 * @param fundingSats          total number of satoshis in the UTXO used for this calculation.
	 * @param redeemPrice          price (units/BCH) to base the redemption simulation on.
	 *
	 * @returns simulationOutput   the payout results of the simulation.
	 *
	 * @private
	 */
	async calculateSettlementOutcome(
		contractParameters: ContractParameters,
		fundingSats: number,
		redeemPrice: number,
	): Promise<SimulationOutput>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'calculateSettlementOutcome() <=', arguments ]);

		// Throw an error if provided fundingSats is not an integer
		if(!Number.isInteger(fundingSats))
		{
			throw(new Error('Provided fundingSats must be an integer'));
		}

		// Throw an error if provided redeemPrice is not an integer
		if(!Number.isInteger(redeemPrice))
		{
			throw(new Error('Provided redeemPrice must be an integer'));
		}

		// calculate the clamped price for the contract outcomes.
		const clampedPrice = Math.max(Math.min(redeemPrice, contractParameters.highLiquidationPrice), contractParameters.lowLiquidationPrice);

		// Calculate hedge payout and dust protection
		// Use floor on the payout calculation to match the reality of the contract math which does integer math
		const hedgePayoutSatsUnsafe = Math.floor(contractParameters.nominalUnitsXSatsPerBch / clampedPrice);
		const hedgePayoutSatsSafe = Math.max(DUST_LIMIT, hedgePayoutSatsUnsafe);

		// Calculate long payout and dust protection, and total payout
		const longPayoutSatsUnsafe = contractParameters.payoutSats - hedgePayoutSatsSafe;
		const longPayoutSatsSafe = Math.max(DUST_LIMIT, longPayoutSatsUnsafe);

		// Calculate total payout with dust safety and final miner fee
		const totalPayoutSatsSafe = hedgePayoutSatsSafe + longPayoutSatsSafe;
		const minerFeeSats = fundingSats - totalPayoutSatsSafe;

		const result = { hedgePayoutSatsSafe, longPayoutSatsSafe, totalPayoutSatsSafe, minerFeeSats };

		// Write log entry for easier debugging.
		debug.action('Simulating contract outcomes.');

		// Output function result for easier collection of test data.
		debug.result([ 'calculateSettlementOutcome() =>', result ]);

		// Return the results of the calculation.
		return result;
	}

	/**
	 * Builds a contract instance from contract parameters.
	 *
	 * @param contractParameters   contract parameters required to build the contract instance.
	 *
	 * @returns a contract instance.
	 *
	 * @private
	 */
	async compileContract(contractParameters: ContractParameters): Promise<Contract>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'compileContract() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Creating contract instance.');

		// Retrieve the correct artifact.
		const artifact = AnyHedgeArtifacts[this.contractVersion];

		// Convert contract parameters object into ordered list of contract constructor arguments
		const parameters =
		[
			contractParameters.hedgeMutualRedeemPublicKey,
			contractParameters.longMutualRedeemPublicKey,
			contractParameters.enableMutualRedemption,
			contractParameters.hedgeLockScript,
			contractParameters.longLockScript,
			contractParameters.oraclePublicKey,
			contractParameters.nominalUnitsXSatsPerBch,
			contractParameters.payoutSats,
			contractParameters.lowLiquidationPrice,
			contractParameters.highLiquidationPrice,
			contractParameters.startTimestamp,
			contractParameters.maturityTimestamp,
		];

		// Instantiate the contract
		const contract = new Contract(artifact, parameters, this.networkProvider);

		// Output function result for easier collection of test data.
		debug.result([ 'compileContract() =>', `${contract.name} contract with address ${contract.address}` ]);

		// Pass back the contract to the caller.
		return contract;
	}

	/**
	 * Creates a new contract.
	 *
	 * @param contractCreationParameters  {ContractCreationParameters}   object containing the parameters to create this contract.
	 *
	 * @returns {Promise<ContractData>} the contract parameters and metadata.
	 */
	async createContract(contractCreationParameters: ContractCreationParameters): Promise<ContractData>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'createContract() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Preparing a new contract.');

		// Extract relevant parameters.
		const {
			takerSide,
			makerSide,
			oraclePublicKey,
			hedgePayoutAddress,
			longPayoutAddress,
			enableMutualRedemption,
			nominalUnits,
			startingOracleMessage,
			startingOracleSignature,
			maturityTimestamp,
			highLiquidationPriceMultiplier,
			lowLiquidationPriceMultiplier,
			hedgeMutualRedeemPublicKey,
			longMutualRedeemPublicKey,
		} = contractCreationParameters;

		// Throw an error if the taker is invalid.
		if(!(takerSide === 'hedge' || takerSide === 'long'))
		{
			throw(new Error(`Taker (${takerSide}) must be either 'hedge' or ' long'.`));
		}

		// Throw an error if maker side if not opposite to taker side.
		if(makerSide !== (takerSide === 'hedge' ? 'long' : 'hedge'))
		{
			throw(new Error(`Maker (${makerSide}) must be on the opposite side of the taker (${takerSide})`));
		}

		// Validate the starting oracle message.
		await OracleData.verifyMessageSignature(hexToBin(startingOracleMessage), hexToBin(startingOracleSignature), hexToBin(oraclePublicKey));

		// Extract starting time and price from the starting oracle message.
		const { messageTimestamp: startTimestamp, priceValue: startPrice } = await OracleData.parsePriceMessage(hexToBin(startingOracleMessage));

		// There are 4 root numbers from which other values are derived.
		// 1. Low liquidation price.
		// 2. High liquidation price
		// 3. Hedge input satoshis.
		// 4. Composite value representing Hedge's unit value.

		// 1. Low liquidation price: the low price that triggers liquidation.
		// The value is rounded to achieve a result as close as possible to intent.
		// More strict terms such as floor and ceiling are imposed on derivative values.
		const lowLiquidationPrice = Math.round(lowLiquidationPriceMultiplier * startPrice);

		// 2. High liquidation price: the high price that triggers liquidation.
		// The value is rounded to achieve a result as close as possible to intent.
		const highLiquidationPrice = Math.round(highLiquidationPriceMultiplier * startPrice);

		// 3. Hedge input satoshis.
		// Hedge: Satoshis equal to the hedged unit value at the start price.
		//        The value is rounded to achieve a result as close as possible to intent.
		//        More strict terms such as floor and ceiling are imposed on derivative values.
		// For readability, we also derive the naive values of total and long satoshis which will be adjusted later.
		// Total: Satoshis equal to the hedged unit value at the low liquidation price. I.e. long gets about zero.
		//        The value is ceiling to ensure that the result is *at least* enough to cover hedge value, never less.
		// Long:  Satoshis equal to difference between the total satoshis and hedge satoshis.
		//        The value is recorded for metadata purposes only.
		const hedgeInputInSatoshis = Math.round((nominalUnits * SATS_PER_BCH) / startPrice);
		const totalInputSats = Math.ceil((nominalUnits * SATS_PER_BCH) / lowLiquidationPrice);

		// 4. Composite number representing Hedge's unit value.
		// The number is calculated as hedge units * 1e8 sats/bch.
		// This allows the final calculation in the contract to be simple division, and is also a carryover
		// from previous BCH VM versions (before May 2022) that did not have multiplication.
		// The value divided by the price in BCH directly yields satoshis for hedge value at said price.
		// The value is rounded to achieve a result as close as possible to intent.
		// More strict terms such as floor and ceiling are imposed on derivative values.
		const nominalUnitsXSatsPerBch = Math.round(nominalUnits * SATS_PER_BCH);

		// After the 4 root values, we derive the remaining money-related numbers for the contract and metadata.

		// Total sats, long input sats, long input units and hedge input units are calculated only for metadata
		const longInputInSatoshis = totalInputSats - hedgeInputInSatoshis;
		const longInputInOracleUnits = ((longInputInSatoshis / SATS_PER_BCH) * startPrice);
		const hedgeInputInOracleUnits = ((hedgeInputInSatoshis / SATS_PER_BCH) * startPrice);

		// In addition to money-related numbers, we derive time-related numbers for the contract and metadata.

		// Calculate the contracts duration in seconds.
		const durationInSeconds = maturityTimestamp - startTimestamp;

		// We also package keys and other fixed values for the contract and metadata.

		// Create hedge and long lock scripts from the provided addresses.
		const hedgeLockScript = addressToLockScript(hedgePayoutAddress);
		const longLockScript = addressToLockScript(longPayoutAddress);

		// Assemble the contract parameters.
		const contractParameters =
		{
			maturityTimestamp,
			startTimestamp,
			highLiquidationPrice,
			lowLiquidationPrice,
			payoutSats: totalInputSats,
			nominalUnitsXSatsPerBch,
			oraclePublicKey,
			longLockScript,
			hedgeLockScript,
			enableMutualRedemption,
			longMutualRedeemPublicKey,
			hedgeMutualRedeemPublicKey,
		};

		// Validate safety constraints that keep contracts redeemable, and policy constraints that keep contracts sane.
		await validateLowLiquidationPrice(
			contractParameters.lowLiquidationPrice,
			startPrice,
		);
		await validateHighLiquidationPrice(
			contractParameters.highLiquidationPrice,
			startPrice,
		);
		await validateNominalUnitsXSatsPerBch(
			contractParameters.nominalUnitsXSatsPerBch,
			contractParameters.highLiquidationPrice,
			contractParameters.lowLiquidationPrice,
			contractParameters.payoutSats,
		);
		await validatePayoutSats(contractParameters.payoutSats);

		// Build the corresponding contract
		const contract = await this.compileContract(contractParameters);

		// Estimate the miner cost for the payout transaction size (paying 1.0 sats/b).
		const minerCostInSatoshis = await estimatePayoutTransactionFee(contract, contractParameters, 1.0);

		// Assemble the contract metadata.
		const contractMetadata: ContractMetadata =
		{
			takerSide,
			makerSide,
			hedgePayoutAddress,
			longPayoutAddress,
			startingOracleMessage,
			startingOracleSignature,
			durationInSeconds,
			highLiquidationPriceMultiplier,
			lowLiquidationPriceMultiplier,
			startPrice,
			nominalUnits,
			hedgeInputInOracleUnits,
			longInputInOracleUnits,
			hedgeInputInSatoshis,
			longInputInSatoshis,
			minerCostInSatoshis,
		};

		// Assemble the final contract data.
		const contractData =
		{
			version: this.contractVersion,
			address: contract.address,
			parameters: contractParameters,
			metadata: contractMetadata,
			fundings: [],
			fees: [],
		};

		// Output function result for easier collection of test data.
		debug.result([ 'createContract() =>', contractData ]);

		// Pass back the contract data to the caller.
		return contractData;
	}

	/**
	 * Adds a new fee to a set of contract data, allowing services to define their fees before registering with a settlement service.
	 *
	 * @param contractData   contract data to add a fee to.
	 * @param contractFee    the fee to add to the contract data.
	 *
	 * @returns an updated contractData including the added fee.
	 */
	async addContractFee(contractData: ContractData, contractFee: ContractFee): Promise<ContractData>
	{
		// Extract fee details for legibility.
		const { address, satoshis, name, description } = contractFee;

		// Throw an error if the fee is below the dust limit.
		if(satoshis < DUST_LIMIT)
		{
			throw(new Error(`Cannot add the '${name}' fee of ${satoshis} satoshis, as it is below the dust limit (${DUST_LIMIT}).`));
		}

		// Throw an error if there is already an existing fee matching the name and address.
		const existingFee = contractData.fees.find((fee) => (fee.name === name && fee.address === address));
		if(existingFee)
		{
			throw(new Error(`Cannot add the '${name}' fee to '${address}' as a fee for that name and address already exist.`));
		}

		// Add the fee to the contract data. Using sanitized data. Don't send in contractFee.
		contractData.fees.push({ address, satoshis, name, description });

		// Return the updated contract data.
		return contractData;
	}

	/**
	 * Parse a settlement transaction to extract as much data as possible, ending up with
	 * ContractParameters, ContractSettlement and ContractFunding objects, depending on what data
	 * could be retrieved.
	 *
	 * @param settlementTransactionHex   hex string for the settlement transaction
	 *
	 * @throws {Error} when the passed transaction hex cannot be parsed by Libauth.
	 * @throws {SettlementParseError} if the transaction does not have exactly one input.
	 * @throws {SettlementParseError} if the transaction does not have exactly two outputs.
	 * @throws {SettlementParseError} if the unlocking script does not include exactly 6 or 10 input parameters.
	 * @throws {SettlementParseError} if the redeem script does not match expectations for an AnyHedge contract.
	 * @returns ContractParameters, ContractSettlement, and ContractFunding objects. See {@link https://gitlab.com/GeneralProtocols/anyhedge/library/-/blob/development/examples/parse-settlement-transaction.js|examples/parse-settlement-transaction.js} to inspect the data that this function returns.
	 */
	async parseSettlementTransaction(settlementTransactionHex: string): Promise<ParsedSettlementData>
	{
		return parseSettlementTransaction(settlementTransactionHex);
	}
}
