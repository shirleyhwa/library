export const DEFAULT_SERVICE_DOMAIN = 'api.anyhedge.com';
export const DEFAULT_SERVICE_PORT = 443;
export const DEFAULT_SERVICE_SCHEME = 'https';
export const DEFAULT_CONTRACT_VERSION = 'AnyHedge v0.11';
export const CONSTRUCTOR_PARAMETER_COUNT = 12;
export const DUST_LIMIT = 546;
export const SATS_PER_BCH = 100000000;

// This is intended to be used for funding transaction size estimation
export const P2PKH_INPUT_SIZE = 148;

// todo: all ok in diagram and tests (don't appear in tests)
// Since May 2022 BCH upgrade, script int max is 2^63-1. However, javascript integers become unreliable around 53 bits.
// For example, 2^63-1 in javascript results in a number that is *larger* than the correct value.
// The largest javascript integer that is smaller than the actual limit is 9223372036854775000.
// This number must only be used for strict limit tests on numbers that will not be used in calculations.
// Documented in constraints diagram: "X = MAX_INT" (this value is a strictly safer derivative of the diagram constraint)
export const JAVASCRIPT_FRIENDLY_SCRIPT_INT_HARD_MAX = 9223372036854775000;

// Javascript can retain integer precision in calculations up to about 53 bits without bigint.
// It is important for this library to have integer-precision in calculations so that the outcome of contract
// simulations will match the numerical results of the VM. A better solution is to use bigint eventually.
// Therefore, we limit script numbers that will be used in calculations, specifically integer divisions, to 53 bits,
// still a large number > 10^15.
// Documented in constraints diagram: "X = MAX_INT" (this value is a strictly safer derivative of the diagram constraint)
export const JAVASCRIPT_FRIENDLY_SCRIPT_INT_MAX = 2 ** 53;

// Default minimum division precision in terms of the number of steps required between (numerator / denominator).
// (1 / this value) represents the worst case % error due to a contract division operation.
// E.g. 500 implies 1/500 = 0.2% worst case calculation error.
// This is not strictly a safety constraint, but allows for stricter reasoning among the safety constraints.
// It should also satisfy these common-sense constraints:
//     Min: X >= 1
//     Max: None. Too-large values will just cause all contracts to fail creation.
// Documented in constraints diagram: "X = MIN_DIV_PRECISION_STEPS"
export const MIN_INTEGER_DIVISION_PRECISION_STEPS = 500;

// Maximum satoshis allowed in a contract.
// This is not strictly a safety constraint, but allows for more meaningful reasoning among the safety constraints.
// It must satisfy these safety constraints:
//     Min: X >= DUST
//     Max: X < 10m or whatever technically possible portion of total 21m circulation <= JAVASCRIPT_FRIENDLY_SCRIPT_INT_MAX
// Documented in constraints diagram: "X = MAX_SATS"
export const MAX_CONTRACT_SATS = Math.round(10_000 * SATS_PER_BCH);

// Minimum relative change from start price to the high and low liquidation prices.
// For example, 0.005 means there must be at least a 0.5% change from start price to each of high and low prices.
// This is a policy constraint, and it must should also satisfy these common-sense constraints:
//     Min: X > 0
//     Max: X < 1
// Not documented in constraints diagram as this is purely a policy constraint.
export const MIN_LIQUIDATION_RELATIVE_PRICE_CHANGE = 0.005;
