import { AutomatedPayoutParametersV1, ContractCreationParametersV1, ContractRegistrationParametersV1, ContractSettlementParametersV1, ContractValidationParametersV1 } from './versions/v1/index.js';

// Combine the interfaces of the different versions
export type ContractCreationParameters = ContractCreationParametersV1;
export type ContractRegistrationParameters = ContractRegistrationParametersV1;
export type ContractValidationParameters = ContractValidationParametersV1;
export type ContractSettlementParameters = ContractSettlementParametersV1;
export type AutomatedPayoutParameters = AutomatedPayoutParametersV1;
