import type { ContractSide, ContractFee } from './index.js';
import type { ContractCreationParametersV1 } from './versions/v1/index.js';
import type { SettlementServiceConfiguration } from './settlement-service.js';
import type { OracleRelayConfiguration } from './oracle-relay.js';

export interface LiquidityConstraints
{
	/** The smallest pre-approved contract size in nominal units according to the oracle */
	minimumNominalUnits: number;

	/** The largest pre-approved contract size in nominal units according to the oracle */
	maximumNominalUnits: number;

	/** The shortest pre-approved contract duration in seconds */
	minimumDurationInSeconds: number;

	/** The longest pre-approved contract duration in seconds */
	maximumDurationInSeconds: number;

	/** The lowest pre-approved liquidation limit, as a percentage of the starting price */
	minimumLiquidationLimit: number;

	/** The highest pre-approved liquidation limit, as a percentage of the starting price */
	maximumLiquidationLimit: number;
}

export interface LiquidityConstraintsPerOracleAndSide
{
	[oraclePublicKey: string]:
	{
		/** Liquidity constraints for Hedge Positions. */
		hedge?: LiquidityConstraints;

		/** Liquidity constraints for Long Positions. */
		long?: LiquidityConstraints;
	};
}

/*
 * Interface used for the /api/v1/liquidityServiceInformation GET response.
 */
export interface ServiceInformationFromLiquidityProvider
{
	/* Contact information for the settlement service that this liquidity provider uses. */
	settlementService: SettlementServiceConfiguration;

	/* Contact information for the oracle relay service that this liquidity provider uses. */
	oracleRelay: OracleRelayConfiguration;

	/* Parameters determining what contract positions this liquidity provider is interested in. */
	liquidityParameters: LiquidityConstraintsPerOracleAndSide;
}

/*
 * Interface used for the /api/v1/prepareContractPosition POST request.
 */
export interface RequestForPayoutDetailsToLiquidityProvider
{
	/** The oracle public key used to identify which liquidity provider pool to request payout details for. */
	oraclePublicKey: string;

	/** The side of the pool (hedge or long) used to identify which liquidity provider pool to request payout details for. */
	poolSide: ContractSide;
}

/*
 * Interface used for the /api/v1/prepareContractPosition POST response.
 */
export interface PayoutDetailsFromLiquidityProvider
{
	/** Public key for mutual redemptions for the liquidity pool, as a hex-encoded string */
	liquidityProvidersMutualRedemptionPublicKey: string;

	/** Payout address where the liquidity provider wants the contract to send funds when settled, as a hex-encoded string */
	liquidityProvidersPayoutAddress: string;

	/** The current limit of liquidity that the liquidity provider is willing to use for this contract position. */
	availableLiquidityInSatoshis: number;
}

/*
 * Interface used for the /api/v1/proposeContract POST request.
 */
export interface ContractProposalToLiquidityProvider
{
	/** The desired contract parameters to propose to the liquidity provider */
	contractCreationParameters: ContractCreationParametersV1;

	/** Optional service fees from the contract proposer. */
	fees?: Array<ContractFee>;
}

// This response is sent with a 200 code as a response to the contract proposal to indicate that the liquidity provider needs a fee to continue.
export interface LiquidityFees
{
	/** The fee that will be charged by the Liquidity Provider, in satoshis. */
	liquidityProviderFeeInSatoshis: number;

	/** The timestamp at which point the liqudity provider would prefer to renegotiate the terms. */
	renegotiateAfterTimestamp: number;
}

// This response is sent with a 400 code as a response to the contract proposal to indicate that there is not enough liquidity available for the proposed position.
export interface LiquidityLimit
{
	/** The current limit of liquidity that the liquidity provider is willing to use for this contract position. */
	availableLiquidityInSatoshis: number;
}

/*
 * Interface used for the /api/v1/proposeContract POST response.
 */
export type ContractProposalResponseFromLiquidityProvider = LiquidityFees | LiquidityLimit;

/*
 * Interface used for the /api/v1/fundContract POST request.
 *
 * NOTE: the outpoint provided (transaction hash, index and satoshis) by the user
 *       will be validated when a funding transaction is broadcasted to the network.
 */
export interface FundingOfferToLiquidityProvider
{
	/** The address of the contract. */
	contractAddress: string;

	/** The transaction hash of the Outpoint (UTXO) being spent. */
	outpointTransactionHash: string;

	/** The index of the Outpoint (UTXO) being spent. */
	outpointIndex: number;

	/** The satoshis locked in the Outpoint (UTXO). */
	satoshis: number;

	/** The signature to provide to the Liquidity Provider so that the Contract can be funded. */
	signature: string;

	/** The public key that corresponds to the signature. */
	publicKey: string;

	/** The position being taken (hedge or long) in this proposal. */
	takerSide: ContractSide;

	/** Array of transaction hashes that the outpoint depends upon. */
	dependencyTransactions: string[];
}

/*
 * Interface used for the /api/v1/fundContract POST response.
 */
export interface CompletedFundingFromLiquidityProvider
{
	/** The transaction has of the broadcasted funding transaction. */
	fundingTransactionHash: string;
}
