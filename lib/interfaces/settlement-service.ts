 /**
 * Configuration parameters used to communicate with a settlement service.
 */
export interface SettlementServiceConfiguration
{
	/** The host (domain) of the Service. */
	host: string;

	/** The port number of the Service. */
	port: number;

	/** The scheme (http or https) of the Service. */
	scheme: 'http' | 'https';
}
