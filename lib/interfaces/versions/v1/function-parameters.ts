import { ContractMetadataV1, ContractParametersV1, ContractFundingV1, ContractSideV1 } from './contract-data.js';

/**
 * Parameters required to create an AnyHedge contract.
 */
export interface ContractCreationParametersV1
{
	/** Which side of the contract that the initiating party takes on. */
	takerSide: ContractSideV1;

	/** Which side of the contract that the responding party takes on. */
	makerSide: ContractSideV1;

	/** Intended nominal size of the contract, also represents the intended hedge size of the contract. */
	nominalUnits: number;

	/** Public key of the oracle that provide price information to this contract. */
	oraclePublicKey: string;

	/** Oracle message used as the starting point for the contract. */
	startingOracleMessage: string;

	/** Oracle signature for the message used as the starting point for the contract. */
	startingOracleSignature: string;

	/** Unix timestamp of the date and time at which the contract matures. */
	maturityTimestamp: number;

	/** Multiplier for startPrice that determines the lower liquidation price, and therefor both the leverage and price protection of the contract. */
	lowLiquidationPriceMultiplier: number;

	/** Multiplier for startPrice that determines the upper liquidation price. */
	highLiquidationPriceMultiplier: number;

	/** Payout address where the hedge position of the contract pays out to on settlement. */
	hedgePayoutAddress: string;

	/** Payout address where the long position of the contract pays out to on settlement. */
	longPayoutAddress: string;

	/** Integer flag that can be used to enable or disable mutual redemption support in the contract. (0 = disabled, 1 = enabled) */
	enableMutualRedemption: number;

	/** Public key for the hedge positions keypair used for mutual redemption. */
	hedgeMutualRedeemPublicKey: string;

	/** Public key for the long positions keypair used for mutual redemption. */
	longMutualRedeemPublicKey: string;
}

/**
 * Parameters required when registering contracts with a settlement service.
 */
export type ContractRegistrationParametersV1 = ContractCreationParametersV1;

/**
 * Parameters required when validating that a contract address corresponds to the given contract creation parameters.
 */
export interface ContractValidationParametersV1 extends ContractCreationParametersV1
{
	/** Address of the contract to verify parameterization for. */
	contractAddress: string;
}

/**
 * Parameters required to mature or liquidate an AnyHedge contract.
 */
export interface ContractSettlementParametersV1
{
	/** Public key of the oracle that provide price information to this contract. */
	oraclePublicKey: string;

	/** Oracle message that should be used to settle this contract. */
	settlementMessage: string;

	/** Oracle signature for the message that should be used to settle this contract. */
	settlementSignature: string;

	/** Oracle message used to verify that the settlement message is the first possible message to settle with. */
	previousMessage: string;

	/** Oracle signature for the message used to verify that the settlement message is the first possible message to settle with. */
	previousSignature: string;

	/** The specific contract funding for this contract that should be settled. */
	contractFunding: ContractFundingV1;

	/** Contract metadata necessary to calculate the payout satoshis. */
	contractMetadata: ContractMetadataV1;

	/** Contract parameters necessary to execute and instance of the contract. */
	contractParameters: ContractParametersV1;
}

export interface AutomatedPayoutParametersV1
{
	/** Public key of the oracle that provide price information to this contract. */
	oraclePublicKey: string;

	/** Oracle message that should be used to settle this contract. */
	settlementMessage: string;

	/** Oracle signature for the message that should be used to settle this contract. */
	settlementSignature: string;

	/** Oracle message used to verify that the settlement message is the first possible message to settle with. */
	previousMessage: string;

	/** Oracle signature for the message used to verify that the settlement message is the first possible message to settle with. */
	previousSignature: string;

	/**  Number of satoshis to pay out to the hedge side of the contract. */
	hedgePayoutSats: number;

	/**  Number of satoshis to pay out to the long side of the contract. */
	longPayoutSats: number;

	/** The specific contract funding for this contract that should be settled. */
	contractFunding: ContractFundingV1;

	/** Contract parameters necessary to execute and instance of the contract. */
	contractParameters: ContractParametersV1;

	/** Number of satoshis to use for the miner fee for the payout transaction */
	minerFeeSats: number;
}
