import { debug } from './javascript-util.js';
import { DUST_LIMIT, CONSTRUCTOR_PARAMETER_COUNT } from '../constants.js';
import { ContractFunding, ContractMetadata, ContractParameters, ParsedFunding, ParsedSettlementData, SettlementType } from '../interfaces/index.js';
import {  deriveHdPrivateNodeFromSeed, hexToBin, binToHex, decodeTransactionUnsafe, AuthenticationInstructionPush, Output, OpcodesBCH, decodeAuthenticationInstructions, hashTransactionUiOrder } from '@bitauth/libauth';
import { addressToLockScript, buildLockScriptP2PKH, buildLockScriptP2SH, lockScriptToAddress } from './bitcoincash-util.js';
import { Contract, Transaction, Utxo, utils as cashscriptUtils } from 'cashscript';
import { OracleData } from '@generalprotocols/price-oracle';
import { SettlementParseError } from '../errors.js';
import { AnyHedgeArtifacts } from '@generalprotocols/anyhedge-contracts';

/**
* Helper function to calculate the total sats for a contract (inputs, miner fees, dust cost)
*
* @param contractMetadata   a ContractMetadata object including the sats information for a contract.
*
* @returns the number of total sats that are contained in the contract.
* @private
*/
export const calculateTotalSats = function(contractMetadata: ContractMetadata): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'calculateTotalSats() <=', arguments ]);

	// Calculate total sats
	const { hedgeInputInSatoshis, longInputInSatoshis, minerCostInSatoshis } = contractMetadata;
	const totalSats = hedgeInputInSatoshis + longInputInSatoshis + minerCostInSatoshis + DUST_LIMIT;

	// Output function result for easier collection of test data.
	debug.result([ 'calculateTotalSats() =>', totalSats ]);

	return totalSats;
};

/**
 * Generates a private key hex string used in the redemption of an AnyHedge contract
 * with the provided address
 *
 * @param address   the contract's address
 *
 * @returns the private key used to redeem the AnyHedge contract
 * @private
 */
export const deriveRedemptionKeyFromAddress = function(address: string): string
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'deriveRedemptionKeyFromAddress() <=', arguments ]);

	// Use the contract's lock script as the wallet seed
	const seed = hexToBin(addressToLockScript(address));

	// Derive an HD node using the seed and extract its private key
	const hdNode = deriveHdPrivateNodeFromSeed(seed, true);
	const privateKey = binToHex(hdNode.privateKey);

	// Output function result for easier collection of test data.
	debug.result([ 'deriveRedemptionKeyFromAddress() =>', privateKey ]);

	return privateKey;
};

/**
 * Convert a CashScript UTXO to a ContractFunding interface.
 *
 * @param coin   CashScript UTXO to be converted.
 *
 * @returns ContractFunding interface.
 * @private
 */
export const contractCoinToFunding = function(coin: Utxo): ContractFunding
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'contractCoinToFunding() <=', arguments ]);

	// Format the CashScript UTXO to match the ContractFunding interface
	const funding =
	{
		fundingTransactionHash: coin.txid,
		fundingOutputIndex: coin.vout,
		fundingSatoshis: coin.satoshis,
	};

	// Output function result for easier collection of test data.
	debug.result([ 'contractCoinToFunding() =>', funding ]);

	return funding;
};

/**
 * Convert a ContractFunding interface to a CashScript UTXO.
 *
 * @param funding   contract funding interface.
 *
 * @returns CashScript UTXO.
 * @private
 */
export const contractFundingToCoin = function(funding: ContractFunding): Utxo
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'contractFundingToCoin() <=', arguments ]);

	// Format the ContractFunding to match the CashScript UTXO interface
	const coin =
	{
		txid: funding.fundingTransactionHash,
		vout: funding.fundingOutputIndex,
		satoshis: funding.fundingSatoshis,
	};

	// Output function result for easier collection of test data.
	debug.result([ 'contractFundingToCoin() =>', coin ]);

	return coin;
};

/**
 * @param funding   contract funding.
 *
 * @returns contract funding UTXO's outpoint as a hex string.
 * @private
 */
export const contractFundingToOutpoint = function(funding: ContractFunding): string
{
	return `${funding.fundingTransactionHash}:${funding.fundingOutputIndex}`;
};

/**
 * Build a CashScript Transaction instance for a payout transaction.
 *
 * @param contract              CashScript contract to use for building the transaction.
 * @param contractParameters    contract parameters to use in the payout transaction.
 * @param contractFunding       contract funding to spend from in the payout transaction.
 * @param settlementMessage     oracle price message used in the payout transaction.
 * @param settlementSignature   signature over the oracle message.
 * @param previousMessage       oracle price message used in the payout transaction.
 * @param previousSignature     signature over the oracle message.
 * @param hedgePayoutSats       amount to be paid out to the hedge.
 * @param longPayoutSats        amount to be paid out to the long.
 * @param minerFeeSats          amount to be used for miner fee.
 *
 * @returns a CashScript Transaction for a payout transaction with the correct details.
 * @private
 */
export const buildPayoutTransaction = async function(
	{
		contract,
		contractFunding,
		contractParameters,
		hedgePayoutSats,
		longPayoutSats,
		previousMessage,
		previousSignature,
		settlementMessage,
		settlementSignature,
		minerFeeSats,
	}: {
		contract: Contract;
		contractParameters: ContractParameters;
		contractFunding: ContractFunding;
		settlementMessage: string;
		settlementSignature: string;
		previousMessage: string;
		previousSignature: string;
		hedgePayoutSats: number;
		longPayoutSats: number;
		minerFeeSats: number;
	},
): Promise<Transaction>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'buildPayoutTransaction() <=', arguments ]);

	// Convert the contract funding to a CashScript UTXO to be used as an input
	const input = contractFundingToCoin(contractFunding);

	// Derive the hedge and long addresses from the contract parameters.
	const hedgeAddress = lockScriptToAddress(contractParameters.hedgeLockScript);
	const longAddress = lockScriptToAddress(contractParameters.longLockScript);

	// Build the payout transaction with locktime specifically set to 0 to override
	// CashScript's default behavior to set locktime to the latest block.
	// NOTE: We set the miner fee manually since cashscript fee estimation is too conservative which causes off-by-one satoshi errors.
	const payoutTransaction = contract.functions
		.payout(
			// Oracle data
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
		)
		.from(input)
		.withoutChange()
		.to(hedgeAddress, hedgePayoutSats)
		.to(longAddress, longPayoutSats)
		.withTime(0)
		.withHardcodedFee(minerFeeSats);

	// Output function result for easier collection of test data.
	debug.result([ 'buildPayoutTransaction() =>', payoutTransaction ]);

	return payoutTransaction;
};

/**
 * Estimate the required miner fee to execute a redemption transaction for a contract.
 *
 * @param contract             CashScript contract instance to use in the estimation.
 * @param contractParameters   contract parameters to use in the estimation.
 * @param feeRate              amount of satoshis to pay per byte of transaction data.
 *
 * @returns an estimate of the payout transaction size for this contract.
 * @private
 */
export const estimatePayoutTransactionFee = async function(
	contract: Contract,
	contractParameters: ContractParameters,
	feeRate: number = 1.0,
): Promise<number>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'estimatePayoutTransactionFee() <=', arguments ]);

	// Create placeholder oracle data.
	const placeholderOracleMessage = binToHex(await OracleData.createPriceMessage(1, 1, 1, 1));
	const placeholderOracleSignature = '00'.repeat(64);

	// Create a placeholder 10 BCH funding (input/output values do not matter for the size calculation).
	const placeholderFunding =
	{
		fundingTransactionHash: '00'.repeat(32),
		fundingOutputIndex: 0,
		fundingSatoshis: 10e8,
	};

	// Send 4 BCH to both hedge and long (input/output values do not matter for the size calculation).
	const placeholderHedgePayoutSats = 4e8;
	const placeholderLongPayoutSats = 4e8;

	// Use sensible 1000 sats placeholder for the miner fee.
	const placeholderMinerFeeInSats = 1000;

	// Build the placeholder payout transaction.
	const payoutTransaction = await buildPayoutTransaction(
		{
			contract,
			contractParameters,
			contractFunding: placeholderFunding,
			settlementMessage: placeholderOracleMessage,
			settlementSignature: placeholderOracleSignature,
			previousMessage: placeholderOracleMessage,
			previousSignature: placeholderOracleSignature,
			hedgePayoutSats: placeholderHedgePayoutSats,
			longPayoutSats: placeholderLongPayoutSats,
			minerFeeSats: placeholderMinerFeeInSats,
		},
	);

	// Build the payout transaction as a raw hex string.
	const rawPayoutTransaction = await payoutTransaction.build();

	// The built transaction is a hex string, so we divide its length by 2
	const transactionSize = rawPayoutTransaction.length / 2;

	// Calculate the transaction fee based on size and fee rate.
	const transactionFee = transactionSize * feeRate;

	// Output function result for easier collection of test data.
	debug.result([ 'estimatePayoutTransactionFee() =>', transactionFee ]);

	return transactionSize;
};

/**
 * Extract the constructor parameters from a redeem script.
 *
 * @param redeemScript   redeem script extracted from a settlement transaction.
 *
 * @throws {Error} if the redeem script does not match expectations for an AnyHedge contract.
 * @returns ContractParameters object.
 * @private
 */
const extractConstructorParameters = function(redeemScript: Uint8Array): ContractParameters
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'extractConstructorParameters() <=', arguments ]);

	// Parse redeem script and extract constructor parameters.
	const constructorParameters = decodeAuthenticationInstructions(redeemScript)
		.slice(0, CONSTRUCTOR_PARAMETER_COUNT) as AuthenticationInstructionPush[];
	const [
		maturityTimestamp,
		startTimestamp,
		highLiquidationPrice,
		lowLiquidationPrice,
		payoutSats,
		nominalUnitsXSatsPerBch,
		oraclePublicKey,
		longLockScript,
		hedgeLockScript,
		enableMutualRedemption,
		longMutualRedeemPublicKey,
		hedgeMutualRedeemPublicKey,
	] = constructorParameters;

	// Convert available data into a ContractParameters object.
	// The following parameters are unavailable because they are hidden behind data hashes:
	// oraclePublicKey, hedgeLockScript, longLockScript, hedgeMutualRedeemPublicKey, longMutualRedeemPublicKey.
	const contractParameters =
	{
		lowLiquidationPrice: cashscriptUtils.decodeInt(lowLiquidationPrice.data),
		highLiquidationPrice: cashscriptUtils.decodeInt(highLiquidationPrice.data),
		startTimestamp: cashscriptUtils.decodeInt(startTimestamp.data),
		maturityTimestamp: cashscriptUtils.decodeInt(maturityTimestamp.data),
		oraclePublicKey: binToHex(oraclePublicKey.data),
		hedgeLockScript: binToHex(hedgeLockScript.data),
		longLockScript: binToHex(longLockScript.data),
		hedgeMutualRedeemPublicKey: binToHex(hedgeMutualRedeemPublicKey.data),
		longMutualRedeemPublicKey: binToHex(longMutualRedeemPublicKey.data),
		enableMutualRedemption: enableMutualRedemption.opcode === OpcodesBCH.OP_1 ? 1 : 0,
		nominalUnitsXSatsPerBch: cashscriptUtils.decodeInt(nominalUnitsXSatsPerBch.data),
		payoutSats: cashscriptUtils.decodeInt(payoutSats.data),
	};

	// Output function result for easier collection of test data.
	debug.result([ 'extractConstructorParameters() =>', contractParameters ]);

	return contractParameters;
};

/**
 * Checks whether a passed redeem script hex string belongs to an AnyHedge contract.
 *
 * @param redeemScriptHex   redeem script extracted from a settlement transaction.
 *
 * @returns true if it matches AnyHedge bytecode, false if not.
 * @private
 */
const isAnyHedgeRedeemScript = function(redeemScriptHex: string): boolean
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'isAnyHedgeRedeemScript() <=', arguments ]);

	// Retrieve artifact object. Note that this is hardcoded for AnyHedge v0.11.
	const artifact = AnyHedgeArtifacts['AnyHedge v0.11'];

	// Convert artifact's ASM to hex bytecode.
	const contractBytecode = binToHex(cashscriptUtils.asmToBytecode(artifact.bytecode));

	// Check that the passed redeem script includes AnyHedge bytecode.
	const redeemScriptIncludesAnyHedgeBytecode = redeemScriptHex.includes(contractBytecode);

	// Output function result for easier collection of test data.
	debug.result([ 'isAnyHedgeRedeemScript() =>', redeemScriptIncludesAnyHedgeBytecode ]);

	return redeemScriptIncludesAnyHedgeBytecode;
};

/**
 * Given a list of transaction outputs, and a hedge and long input script, return
 * the payout satoshis for the hedge and long parties.
 *
 * @param outputs              list of transaction outputs to parse
 * @param hedgeLockScriptHex   hedge lock script
 * @param longLockScriptHex    long lock script
 *
 * @returns object containing hedge and long satoshis if they could be parsed from the outputs.
 * @private
 */
const parseTransactionOutputs = function(
	outputs: Output[],
	hedgeLockScriptHex?: string,
	longLockScriptHex?: string,
): { hedgePayoutInSatoshis?: number; longPayoutInSatoshis?: number }
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'parseTransactionOutputs() <=', arguments ]);

	// Declare variables for hedge and long satoshis.
	let hedgePayoutInSatoshis;
	let longPayoutInSatoshis;

	// Loop over all outputs.
	for(const output of outputs)
	{
		// Convert the output's satoshis to a number.
		const outputSatoshis = Number(output.valueSatoshis);

		// Assign the output's satoshis to hedge satoshis if the lock script matches.
		if(binToHex(output.lockingBytecode) === hedgeLockScriptHex)
		{
			hedgePayoutInSatoshis = outputSatoshis;
		}

		// Assign the output's satoshis to long satoshis if the lock script matches.
		if(binToHex(output.lockingBytecode) === longLockScriptHex)
		{
			longPayoutInSatoshis = outputSatoshis;
		}
	}

	// Assemble the extracted satoshis.
	const extractedSatoshis = { hedgePayoutInSatoshis, longPayoutInSatoshis };

	// Output function result for easier collection of test data.
	debug.result([ 'parseTransactionOutputs() =>', extractedSatoshis ]);

	return extractedSatoshis;
};

/**
 * Extract the input parameters for a mutual redemption transaction and build
 * ContractParameters and ContractSettlement objects.
 *
 * @param inputParameters      list of input parameters as PUSH operations.
 * @param transactionHashHex   transaction hash hex string for the settlement transaction.
 * @param outputs              list of transaction outputs.
 * @param funding              contract funding that was already parsed.
 *
 * @throws {SettlementParseError} if the redeem script does not match expectations for an AnyHedge contract.
 * @returns ContractParameters and ContractSettlement objects.
 * @private
 */
const parseMutualRedemptionTransaction = async function(
	transactionHashHex: string,
	inputParameters: AuthenticationInstructionPush[],
	outputs: Output[],
	funding: ParsedFunding,
): Promise<ParsedSettlementData>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'parseMutualRedemptionTransaction() <=', arguments ]);

	// Extract mutual redemption redeem script.
	// Note that input arguments are in push (reverse) order compared to the contract.
	/* eslint-disable @typescript-eslint/naming-convention */
	const [
		_longMutualRedeemSignature,
		_hedgeMutualRedeemSignature,
		_functionSelector,
		redeemScript,
	] = inputParameters;
	/* eslint-enable @typescript-eslint/naming-convention */

	// Check that the redeem script matches AnyHedge bytecode, or throw an error.
	if(!isAnyHedgeRedeemScript(binToHex(redeemScript.data)))
	{
		throw(new SettlementParseError('Settlement redeem script does not match AnyHedge contract bytecode'));
	}

	// Extract constructor parameters from the redeem script.
	const parameters = extractConstructorParameters(redeemScript.data);

	// Parse the transaction outputs to determine the hedge and long payout satoshis.
	const hedgeLockScriptHex = buildLockScriptP2PKH(parameters.hedgeMutualRedeemPublicKey);
	const longLockScriptHex = buildLockScriptP2PKH(parameters.longMutualRedeemPublicKey);
	const { hedgePayoutInSatoshis, longPayoutInSatoshis } = parseTransactionOutputs(outputs, hedgeLockScriptHex, longLockScriptHex);

	// Define settlement data.
	const settlement =
	{
		settlementType: SettlementType.MUTUAL,
		settlementTransactionHash: transactionHashHex,
		hedgePayoutInSatoshis,
		longPayoutInSatoshis,
	};

	// Derive the contract's address.
	const contractLockScript = buildLockScriptP2SH(binToHex(redeemScript.data));
	const address = lockScriptToAddress(contractLockScript);

	// Assemble the extracted data.
	const extractedData = { address, funding, parameters, settlement };

	// Output function result for easier collection of test data.
	debug.result([ 'parseMutualRedemptionTransaction() =>', extractedData ]);

	return extractedData;
};

/**
 * Extract the input parameters for a payout transaction and build
 * ContractParameters and ContractSettlement objects.
 *
 * @param transactionHashHex   transaction hash hex string for the settlement transaction.
 * @param inputParameters      list of input parameters as PUSH operations.
 * @param outputs              list of transaction outputs.
 * @param funding              contract funding that was already parsed.
 *
 * @throws {SettlementParseError} if the redeem script does not match expectations for an AnyHedge contract.
 * @returns ContractParameters and ContractSettlement objects.
 * @private
 */
const parsePayoutTransaction = async function(
	transactionHashHex: string,
	inputParameters: AuthenticationInstructionPush[],
	outputs: Output[],
	funding: ParsedFunding,
): Promise<ParsedSettlementData>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'parsePayoutTransaction() <=', arguments ]);

	// Extract payout input parameters.
	// Note that input arguments are in push (reverse) order compared to the contract.
	/* eslint-disable @typescript-eslint/naming-convention */
	const [
		previousSignature,
		previousMessage,
		settlementSignature,
		settlementMessage,
		_functionSelector,
		redeemScript,
	] = inputParameters;
	/* eslint-enable @typescript-eslint/naming-convention */

	// Check that the redeem script matches AnyHedge bytecode, or throw an error.
	if(!isAnyHedgeRedeemScript(binToHex(redeemScript.data)))
	{
		throw(new SettlementParseError('Settlement redeem script does not match AnyHedge contract bytecode'));
	}

	// Extract constructor parameters from the redeem script.
	const parameters = extractConstructorParameters(redeemScript.data);

	// Parse the settlement message.
	const { messageTimestamp, priceValue } = await OracleData.parsePriceMessage(settlementMessage.data);

	// Determine whether this is a maturation or a liquidation.
	const isMaturation = messageTimestamp >= parameters.maturityTimestamp;
	const settlementType = isMaturation ? SettlementType.MATURATION : SettlementType.LIQUIDATION;

	// Parse the transaction outputs to determine the hedge and long payout satoshis.
	const hedgeLockScriptHex = parameters.hedgeLockScript;
	const longLockScriptHex = parameters.longLockScript;
	const { hedgePayoutInSatoshis, longPayoutInSatoshis } = parseTransactionOutputs(outputs, hedgeLockScriptHex, longLockScriptHex);

	// Assemble the settlement data.
	const settlement =
	{
		settlementType,
		settlementTransactionHash: transactionHashHex,
		hedgePayoutInSatoshis,
		longPayoutInSatoshis,
		oraclePublicKey: parameters.oraclePublicKey,
		settlementMessage: binToHex(settlementMessage.data),
		settlementSignature: binToHex(settlementSignature.data),
		previousMessage: binToHex(previousMessage.data),
		previousSignature: binToHex(previousSignature.data),
		settlementPrice: priceValue,
	};

	// Derive the contract's address.
	const contractLockScript = buildLockScriptP2SH(binToHex(redeemScript.data));
	const address = lockScriptToAddress(contractLockScript);

	// Assemble the extracted data
	const extractedData = { address, funding, parameters, settlement };

	// Output function result for easier collection of test data.
	debug.result([ 'parsePayoutTransaction() =>', extractedData ]);

	return extractedData;
};

/**
 * Parse a settlement transaction to extract as much data as possible, ending up with
 * ContractParameters, ContractSettlement and ContractFunding objects, depending on what data
 * could be retrieved.
 *
 * @param settlementTransactionHex   hex string for the settlement transaction
 *
 * @throws {Error} when the passed transaction hex cannot be parsed by Libauth.
 * @throws {SettlementParseError} if the transaction does not have exactly one input.
 * @throws {SettlementParseError} if the transaction does not have exactly two outputs.
 * @throws {SettlementParseError} if the unlocking script does not include exactly 6 or 10 input parameters.
 * @throws {SettlementParseError} if the redeem script does not match expectations for an AnyHedge contract.
 * @returns ContractParameters, ContractSettlement, and ContractFunding objects.
 * @private
 */
export const parseSettlementTransaction = async function(settlementTransactionHex: string): Promise<ParsedSettlementData>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'parseSettlementTransaction() <=', arguments ]);

	// Decode the passed transaction hex and extract the inputs and outputs
	const { inputs, outputs } = decodeTransactionUnsafe(hexToBin(settlementTransactionHex));

	// Settlement transactions can only have a single input and two outputs.
	// Note that mutual redemptions could have any number of inputs/outputs, but the most
	// common mutual redemptions (refund & early maturation) have 1/2, like payouts.
	// So arbitrary payout transactions cannot be parsed using this function.
	if(inputs.length !== 1)
	{
		throw(new SettlementParseError('Settlement transaction does not have exactly one input'));
	}
	if(outputs.length !== 2)
	{
		throw(new SettlementParseError('Settlement transaction does not have exactly two outputs'));
	}

	// Extract the only input.
	const [ input ] = inputs;

	// Assemble the funding details.
	const funding =
	{
		fundingTransactionHash: binToHex(input.outpointTransactionHash),
		fundingOutputIndex: input.outpointIndex,
	};

	// Extract the only input's unlocking bytecode.
	const { unlockingBytecode } = input;

	// Parse the input bytecode, knowing that unlocking bytecode can only contain data pushes.
	const inputParameters = decodeAuthenticationInstructions(unlockingBytecode) as AuthenticationInstructionPush[];

	// Compute the transaction's transaction hash.
	const transactionHashHex = binToHex(hashTransactionUiOrder(hexToBin(settlementTransactionHex)));

	// The input script for a mutual redemption has 2 parameters + function selector + redeem script.
	const MUTUAL_REDEMPTION_INPUT_PARAMETERS_LENGTH = 4;

	// The input script for a payout has 4 parameters + function selector + redeem script.
	const PAYOUT_INPUT_PARAMETERS_LENGTH = 6;

	let parsedSettlementData;

	if(inputParameters.length === MUTUAL_REDEMPTION_INPUT_PARAMETERS_LENGTH)
	{
		// Parse the mutual redemption transaction.
		parsedSettlementData = parseMutualRedemptionTransaction(transactionHashHex, inputParameters, outputs, funding);
	}
	else if(inputParameters.length === PAYOUT_INPUT_PARAMETERS_LENGTH)
	{
		// parse the payout transaction.
		parsedSettlementData = await parsePayoutTransaction(transactionHashHex, inputParameters, outputs, funding);
	}
	// Any other number of input parameters means that this is not an AnyHedge transaction.
	else
	{
		throw(new SettlementParseError(`Got ${inputParameters.length} input parameters but expected ${MUTUAL_REDEMPTION_INPUT_PARAMETERS_LENGTH} or ${PAYOUT_INPUT_PARAMETERS_LENGTH}`));
	}

	// Output function result for easier collection of test data.
	debug.result([ 'parseSettlementTransaction() =>', parsedSettlementData ]);

	return parsedSettlementData;
};
