/* eslint-disable max-nested-callbacks */
/* eslint-disable no-param-reassign */

// Import testing libraries and utilities
import { evaluateProgram, loadDefaultContractData, createProgramP2PKH } from './test-util.js';

// Import Bitcoin Cash related interfaces
import { AuthenticationErrorCommon, binToHex } from '@bitauth/libauth';

// Import utility function to handle oracle data.
import { OracleData } from '@generalprotocols/price-oracle';

// Import AnyHedge library
import { AnyHedgeManager, ContractData, UnsignedFundingProposal } from '../lib/index.js';
import { DUST_LIMIT } from '../lib/constants.js';

// Import fixture data
import { TAKER_SIDE, MAKER_SIDE, HEDGE_ADDRESS, LONG_ADDRESS, HEDGE_PUBKEY, LONG_PUBKEY, HEDGE_WIF, LONG_WIF, ORACLE_PUBKEY, ORACLE_WIF, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, START_TIMESTAMP, DEFAULT_DURATION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_FEE_ADDRESS } from './fixture/constants.js';
import { calculateRequiredFundingSatoshis } from '../lib/util/funding-util.js';

// Load an AnyHedge manager.
const loadContractManager = function(): AnyHedgeManager
{
	// Set up instance of AnyHedgeManager
	const manager = new AnyHedgeManager();

	return manager;
};

interface ProposalFixture
{
	hedge: UnsignedFundingProposal;
	long: UnsignedFundingProposal;
}

// Generate default funding proposals for a contract
const generateProposalsFor = function(contractData: ContractData): ProposalFixture
{
	// Calculate the total required satoshis
	const totalRequiredSatoshis = calculateRequiredFundingSatoshis(contractData);

	// Have the hedge pay *only* their input satoshis, while the long pays all the rest (inc fees)
	const { hedgeInputInSatoshis } = contractData.metadata;
	const longInputSatoshis = totalRequiredSatoshis - hedgeInputInSatoshis;

	// Create a dummy UTXO with enough value to pay for the hedge side.
	const hedgeUtxo =
	{
		txid: '0000000000000000000000000000000000000000000000000000000000000001',
		vout: 0,
		satoshis: hedgeInputInSatoshis,
	};

	// Create a dummy UTXO with enough value to pay for the long side + all fees.
	const longUtxo =
	{
		txid: '0000000000000000000000000000000000000000000000000000000000000002',
		vout: 0,
		satoshis: longInputSatoshis,
	};

	// Create two proposal objects from that data
	const proposals =
	{
		hedge:
		{
			contractData: contractData,
			utxo: hedgeUtxo,
		},
		long:
		{
			contractData: contractData,
			utxo: longUtxo,
		},
	};

	return proposals;
};

describe('completeFundingProposal()', () =>
{
	let manager: AnyHedgeManager;
	let proposalsWithFees: ProposalFixture;
	let proposalsWithoutFees: ProposalFixture;

	beforeEach(async () =>
	{
		// Load a contract manager
		manager = loadContractManager();

		// Load the default contract data
		const contractDataWithoutFee = await loadDefaultContractData(manager);

		// Generate default proposals for contractData that excludes fees
		proposalsWithoutFees = generateProposalsFor(contractDataWithoutFee);

		// Load contract data that includes fees.
		const contractDataWithFee =
		{
			...contractDataWithoutFee,
			fees:
			[
				{
					name: 'test',
					description: '',
					address: DEFAULT_FEE_ADDRESS,
					satoshis: Math.max(Math.ceil(contractDataWithoutFee.parameters.payoutSats * 0.003), DUST_LIMIT),
				},
			],
		};

		// Generate default proposals for contractData that includes fees
		proposalsWithFees = generateProposalsFor(contractDataWithFee);
	});

	test('should fail when the proposals do not provide enough satoshis', async () =>
	{
		// Generate signed proposals, intentionally using the "long" UTXO twice, which does not contain enough satoshis
		const signedHedgeProposal = await manager.signFundingProposal(HEDGE_WIF, proposalsWithFees.long);
		const signedLongProposal = await manager.signFundingProposal(LONG_WIF, proposalsWithFees.long);

		// Attempt to complete the funding proposal
		const completeFundingProposalCall = manager.completeFundingProposal(signedHedgeProposal, signedLongProposal);

		// Expect the call to fail
		await expect(completeFundingProposalCall).rejects.toThrow(/should be EXACTLY the required satoshis/);
	});

	test('should fail when the proposals provide too many satoshis', async () =>
	{
		// Generate signed proposals, intentionally using the "hedge" UTXO twice, which contains too many satoshis
		const signedHedgeProposal = await manager.signFundingProposal(HEDGE_WIF, proposalsWithFees.hedge);
		const signedLongProposal = await manager.signFundingProposal(LONG_WIF, proposalsWithFees.hedge);

		// Attempt to complete the funding proposal
		const completeFundingProposalCall = manager.completeFundingProposal(signedHedgeProposal, signedLongProposal);

		// Expect the call to fail
		await expect(completeFundingProposalCall).rejects.toThrow(/should be EXACTLY the required satoshis/);
	});

	test('should fail when the proposals are for different contracts', async () =>
	{
		const VALID_ORACLE_MESSAGE = await OracleData.createPriceMessage(START_TIMESTAMP + 10, 1, 1, DEFAULT_START_PRICE);
		const VALID_ORACLE_SIGNATURE = await OracleData.signMessage(VALID_ORACLE_MESSAGE, ORACLE_WIF);

		// Create a contract with 10 seconds added to the default start timestamp
		const creationParameters =
		{
			takerSide: TAKER_SIDE,
			makerSide: MAKER_SIDE,
			oraclePublicKey: ORACLE_PUBKEY,
			hedgePayoutAddress: HEDGE_ADDRESS,
			longPayoutAddress: LONG_ADDRESS,
			nominalUnits: DEFAULT_NOMINAL_UNITS,
			startingOracleMessage: binToHex(VALID_ORACLE_MESSAGE),
			startingOracleSignature: binToHex(VALID_ORACLE_SIGNATURE),
			maturityTimestamp: START_TIMESTAMP + DEFAULT_DURATION,
			highLiquidationPriceMultiplier: DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
			lowLiquidationPriceMultiplier: 1 - DEFAULT_VOLATILITY_PROTECTION,
			enableMutualRedemption: 1,
			hedgeMutualRedeemPublicKey: HEDGE_PUBKEY,
			longMutualRedeemPublicKey: LONG_PUBKEY,
		};
		const contractData = await manager.createContract(creationParameters);

		// Generate proposals for this non-default contract
		const proposals = generateProposalsFor(contractData);

		// Generate signed proposals, intentionally using two separate contract data.
		const signedHedgeProposal = await manager.signFundingProposal(HEDGE_WIF, proposals.hedge);
		const signedLongProposal = await manager.signFundingProposal(LONG_WIF, proposalsWithFees.long);

		// Attempt to complete the funding proposal
		const completeFundingProposalCall = manager.completeFundingProposal(signedHedgeProposal, signedLongProposal);

		// Expect the call to fail
		await expect(completeFundingProposalCall).rejects.toThrow(/Funding proposals do not use the same contract data/);
	});

	test('should fail when proposal is changed after signing', async () =>
	{
		// Generate signed proposals for both the hedge and the long sides
		const signedHedgeProposal = await manager.signFundingProposal(HEDGE_WIF, proposalsWithFees.hedge);
		const signedLongProposal = await manager.signFundingProposal(LONG_WIF, proposalsWithFees.long);

		// Shuffle some satoshis around in the provided UTXOs.
		signedHedgeProposal.utxo.satoshis += 10;
		signedLongProposal.utxo.satoshis -= 10;

		// Attempt to complete the funding proposal
		const transactionHex = await manager.completeFundingProposal(signedHedgeProposal, signedLongProposal);

		// Create Libauth Authentication Programs to test the transaction.
		const hedgeProgram = await createProgramP2PKH(transactionHex, signedHedgeProposal.publicKey, signedHedgeProposal.utxo.satoshis, 0);
		const longProgram = await createProgramP2PKH(transactionHex, signedLongProposal.publicKey, signedLongProposal.utxo.satoshis, 1);

		// Evaluate the programs
		const hedgeResult = await evaluateProgram(hedgeProgram);
		const longResult = await evaluateProgram(longProgram);

		// Check that the hedge program fails on the signature check
		expect(hedgeResult.result).toEqual(AuthenticationErrorCommon.nonNullSignatureFailure);
		expect(longResult.result).toEqual(AuthenticationErrorCommon.nonNullSignatureFailure);
	});

	test('should fail when one proposal includes a fee and the other does not', async () =>
	{
		// Generate signed proposals for both the hedge and the long sides
		// We intentionally use a proposal without fee included for one of the proposals
		const signedHedgeProposal = await manager.signFundingProposal(HEDGE_WIF, proposalsWithoutFees.hedge);
		const signedLongProposal = await manager.signFundingProposal(LONG_WIF, proposalsWithFees.long);

		// Attempt to complete the funding proposal
		const completeFundingProposalCall = manager.completeFundingProposal(signedHedgeProposal, signedLongProposal);

		// Expect the call to fail
		await expect(completeFundingProposalCall).rejects.toThrow(/Funding proposals do not use the same contract data/);
	});

	test('should fund successfully when both parties use the same contract data and contribute enough satoshis', async () =>
	{
		// Generate signed proposals for both the hedge and the long sides
		const signedHedgeProposal = await manager.signFundingProposal(HEDGE_WIF, proposalsWithFees.hedge);
		const signedLongProposal = await manager.signFundingProposal(LONG_WIF, proposalsWithFees.long);

		// Attempt to complete the funding proposal
		const transactionHex = await manager.completeFundingProposal(signedHedgeProposal, signedLongProposal);

		// Create Libauth Authentication Programs to test the transaction.
		const hedgeProgram = await createProgramP2PKH(transactionHex, signedHedgeProposal.publicKey, signedHedgeProposal.utxo.satoshis, 0);
		const longProgram = await createProgramP2PKH(transactionHex, signedLongProposal.publicKey, signedLongProposal.utxo.satoshis, 1);

		// Evaluate the programs
		const hedgeResult = await evaluateProgram(hedgeProgram);
		const longResult = await evaluateProgram(longProgram);

		// Check that the programs evaluated successfully
		expect(hedgeResult.result).toBe(true);
		expect(longResult.result).toBe(true);
	});

	test('should fund successfully when no fee is included', async () =>
	{
		// Generate signed proposals for both the hedge and the long sides
		const signedHedgeProposal = await manager.signFundingProposal(HEDGE_WIF, proposalsWithoutFees.hedge);
		const signedLongProposal = await manager.signFundingProposal(LONG_WIF, proposalsWithoutFees.long);

		// Attempt to complete the funding proposal
		const transactionHex = await manager.completeFundingProposal(signedHedgeProposal, signedLongProposal);

		// Create Libauth Authentication Programs to test the transaction.
		const hedgeProgram = await createProgramP2PKH(transactionHex, signedHedgeProposal.publicKey, signedHedgeProposal.utxo.satoshis, 0);
		const longProgram = await createProgramP2PKH(transactionHex, signedLongProposal.publicKey, signedLongProposal.utxo.satoshis, 1);

		// Evaluate the programs
		const hedgeResult = await evaluateProgram(hedgeProgram);
		const longResult = await evaluateProgram(longProgram);

		// Check that the programs evaluated successfully
		expect(hedgeResult.result).toBe(true);
		expect(longResult.result).toBe(true);
	});
});
