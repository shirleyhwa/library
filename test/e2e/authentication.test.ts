/* eslint-disable max-nested-callbacks */
import { binToHex, encodeTransaction } from '@bitauth/libauth';
import { MissingAuthenticationTokenError, AnyHedgeManager } from '../../lib/index.js';
import { PROD_ORACLE_PUBKEY, DEFAULT_NOMINAL_UNITS, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, HEDGE_PUBKEY, LONG_PUBKEY, START_TIMESTAMP } from '../fixture/constants.js';

// This test file requires a settlement-services server running at localhost:31157 with token rate limiting off

// Define service domain, scheme and port to use for a localhost server.
const serviceScheme = 'http';
const serviceDomain = 'localhost';
const servicePort = 31157;

// Set up AnyHedgeManager without a token, and with an invalid token
const managerWithoutToken = new AnyHedgeManager({ serviceScheme, serviceDomain, servicePort });
const managerWithInvalidToken = new AnyHedgeManager({ authenticationToken: 'INVALID_TOKEN', serviceScheme, serviceDomain, servicePort });

test('can request a new authentication token when providing name', async () =>
{
	// Request a token by providing a name
	const token = await managerWithoutToken.requestAuthenticationToken('dummy name');

	// Check that the returned token is a string
	expect(token).toBeInstanceOf(String);
});

test('cannot request a new authentication token without providing name', async () =>
{
	// Request a token without providing a name and Check that the request fails
	await expect(() => managerWithoutToken.requestAuthenticationToken(undefined as unknown as string))
		.rejects.toThrow();
});

test.skip('cannot request a new authentication token when provided name is empty', async () =>
{
	// Request a token with an empty string as name and Check that the request fails
	await expect(() => managerWithoutToken.requestAuthenticationToken('')).rejects.toThrow();
});

test('cannot register contract with missing or invalid authentication token', async () =>
{
	// Define valid dummy parameters for the register() function
	const dummyParameters =
	[
		PROD_ORACLE_PUBKEY,
		HEDGE_PUBKEY,
		LONG_PUBKEY,
		DEFAULT_NOMINAL_UNITS,
		DEFAULT_START_PRICE,
		START_TIMESTAMP,
		0,
		10,
		DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		1 - DEFAULT_VOLATILITY_PROTECTION,
	];

	// Attempt to register a contract using both manager instances and Check that both requests fail
	// @ts-ignore
	await expect(() => managerWithoutToken.registerContractForSettlement(...dummyParameters))
		.rejects.toThrowError(MissingAuthenticationTokenError);
	// @ts-ignore
	await expect(() => managerWithInvalidToken.registerContractForSettlement(...dummyParameters))
		.rejects.toThrowError('Request to /api/v1/registerContract failed due to undefined authentication token.');
});

test('cannot submit funding transaction with missing or invalid authentication token', async () =>
{
	// Define dummy parameters to the submitFundingTransaction() function
	const dummyTransaction = binToHex(encodeTransaction({ version: 2, inputs: [], outputs: [], locktime: 0 }));
	const dummyAddress = 'bitcoincash:pp5s4jpe5pcjnl0ah2qxe7u5mwy63lz8r5ggm8tstg';

	// Attempt to submit a funding transaction using both managers and Check that both requests fail
	await expect(() => managerWithoutToken.submitFundingTransaction(dummyAddress, dummyTransaction))
		.rejects.toThrowError(MissingAuthenticationTokenError);
	await expect(() => managerWithInvalidToken.submitFundingTransaction(dummyAddress, dummyTransaction))
		.rejects.toThrowError('Request to /api/v1/submitFunding failed due to undefined authentication token.');
});

test('cannot get contract status with missing or invalid authentication token', async () =>
{
	// Define dummy parameters to the getContractStatus() function
	const dummyAddress = 'bitcoincash:pp5s4jpe5pcjnl0ah2qxe7u5mwy63lz8r5ggm8tstg';

	// Attempt to request a new fee address using both managers and Check that both requests fail
	await expect(() => managerWithoutToken.getContractStatus(dummyAddress))
		.rejects.toThrowError(MissingAuthenticationTokenError);
	await expect(() => managerWithInvalidToken.getContractStatus(dummyAddress))
		.rejects.toThrowError('Request to /api/v1/registerContract failed due to undefined authentication token.');
});
