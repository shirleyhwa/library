// Create contract data for specified hedge units, start price and protection
import { binToHex } from '@bitauth/libauth';
import { OracleData } from '@generalprotocols/price-oracle';
import { ContractData, AnyHedgeManager } from '../../lib/index.js';
import { TAKER_SIDE, MAKER_SIDE, START_TIMESTAMP, ORACLE_PUBKEY, HEDGE_ADDRESS, LONG_ADDRESS, HEDGE_PUBKEY, LONG_PUBKEY, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_LIQUIDATION_TIMESTAMP, ORACLE_WIF } from '../fixture/constants.js';
import { calculateTotalSats } from '../../lib/util/anyhedge-util.js';

// Uses constant fixture values for all other parameters of manager.create()
const loadContractData = async function(
	manager: AnyHedgeManager,
	nominalUnits: number,
	startPrice: number,
): Promise<ContractData>
{
	const DISALLOW_MATURITY_FOR_SOME_TIME = 1000000;

	const VALID_ORACLE_MESSAGE = await OracleData.createPriceMessage(START_TIMESTAMP, 1, 1, startPrice);
	const VALID_ORACLE_SIGNATURE = await OracleData.signMessage(VALID_ORACLE_MESSAGE, ORACLE_WIF);

	const creationParameters =
	{
		takerSide: TAKER_SIDE,
		makerSide: MAKER_SIDE,
		oraclePublicKey: ORACLE_PUBKEY,
		hedgePayoutAddress: HEDGE_ADDRESS,
		longPayoutAddress: LONG_ADDRESS,
		nominalUnits: nominalUnits,
		startingOracleMessage: binToHex(VALID_ORACLE_MESSAGE),
		startingOracleSignature: binToHex(VALID_ORACLE_SIGNATURE),
		maturityTimestamp: START_TIMESTAMP + DISALLOW_MATURITY_FOR_SOME_TIME,
		highLiquidationPriceMultiplier: DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier: 1 - DEFAULT_VOLATILITY_PROTECTION,
		enableMutualRedemption: 1,
		hedgeMutualRedeemPublicKey: HEDGE_PUBKEY,
		longMutualRedeemPublicKey: LONG_PUBKEY,
	};

	const contractData = await manager.createContract(creationParameters);

	return contractData;
};

// This test can be used to check network functionality on mainnet.
// It is currently set up to burn money, but we could update it so that it
// doesn't cost money (because we receive the money back from the contract)
// Usage right now:
// 1. Remove '.skip' from the test.
// 2. Run `npm run build`
// 3. Run `./node_modules/.bin/jest test/e2e/network.test.ts --verbose` (Will fail)
// 4. Send around 40k satoshis the logged address
// 5. Run (3) again (Will succeed) - check logged txid
test.skip('should send transaction to the network', async () =>
{
	// Set up anyhedge manager
	const manager = new AnyHedgeManager();

	// Create a 50 USD cent contract
	const NOMINAL_UNITS = 50;

	// Artificially inflate the start price x10, so that it is less expensive to run this test.
	const START_PRICE = 200000;

	// Load contract data
	const contractData = await loadContractData(manager, NOMINAL_UNITS, START_PRICE);

	// Log contract address
	console.log(contractData.address);

	// Set up test oracle data
	const LIQUIDATION_PRICE = 10000;
	const settlementMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 2, 1, LIQUIDATION_PRICE);
	const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
	const previousMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP - 1, 1, 1, LIQUIDATION_PRICE);
	const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

	// Get contract funding to use in liquidation
	const [ contractFunding ] = await manager.getContractFundings(contractData.parameters);

	if(!contractFunding)
	{
		console.log(`Contract needs to be funded with ${calculateTotalSats(contractData.metadata)} satoshis`);
	}

	expect(contractFunding).toBeTruthy();

	const liquidationParameters =
	{
		oraclePublicKey: ORACLE_PUBKEY,
		settlementMessage: binToHex(settlementMessage),
		settlementSignature: binToHex(settlementSignature),
		previousMessage: binToHex(previousMessage),
		previousSignature: binToHex(previousSignature),
		contractFunding: contractFunding,
		contractMetadata: contractData.metadata,
		contractParameters: contractData.parameters,
	};

	// Liquidate contract
	const txid = await manager.liquidateContractFunding(liquidationParameters);

	// Log txid
	console.log(`https://blockchair.com/bitcoin-cash/transaction/${txid}`);
});
