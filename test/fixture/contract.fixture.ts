// Allow line comments so that the ContractFixture definition is more readable
/* eslint-disable line-comment-position, no-inline-comments, @typescript-eslint/comma-dangle */

import { START_TIMESTAMP, MATURITY_TIMESTAMP, DEFAULT_LIQUIDATION_TIMESTAMP } from './constants';
import { AuthenticationErrorCommon } from '@bitauth/libauth';

// Test cases in this file have been generated using emergent_reason's Python simulation tool and/or spreadsheet

export type ContractFixture =
[
	string, // description
	number, // nominalUnits
	number, // startPrice
	number, // volatilityProtection
	number, // maxPriceIncrease
	number, // messageTimestamp
	number, // settlementPrice
	number, // hedgePayout
	number, // longPayout
	(string | true), // expectedResult
];

// These test cases test liquidations using some basic configurations
export const liquidateFixtures: ContractFixture[] =
[
	[ 'should not liquidate when message timestamp is before start timestamp',    100000, 22222, 0.25, 10.0, START_TIMESTAMP - 10,          16667,          546, 546, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price equals start price',                100000, 22222, 0.25, 10.0, DEFAULT_LIQUIDATION_TIMESTAMP, 22222,          546, 546, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is in range (regular size)',        100000, 22222, 0.25, 10.0, DEFAULT_LIQUIDATION_TIMESTAMP, 16668,          546, 546, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is in range (small size)',             100, 22222, 0.25, 10.0, DEFAULT_LIQUIDATION_TIMESTAMP, 16668,          546, 546, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is in range (large size',         10000000, 22222, 0.25, 10.0, DEFAULT_LIQUIDATION_TIMESTAMP, 16668,          546, 546, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is in range (large protection)',    100000, 22222, 0.95, 10.0, DEFAULT_LIQUIDATION_TIMESTAMP,  1112,          546, 546, AuthenticationErrorCommon.failedVerify ],
	[ 'should not liquidate when oracle price is in range (small protection)',    100000, 22222, 0.05, 10.0, DEFAULT_LIQUIDATION_TIMESTAMP, 21112,          546, 546, AuthenticationErrorCommon.failedVerify ],
	[ 'should liquidate when oracle price is below price (regular size)',         100000, 22222, 0.25, 10.0, DEFAULT_LIQUIDATION_TIMESTAMP, 16667,    599988000, 546, true ],
	[ 'should liquidate when oracle price is below price (small size)',              100, 22222, 0.25, 10.0, DEFAULT_LIQUIDATION_TIMESTAMP, 16667,       599988, 546, true ],
	[ 'should liquidate when oracle price is below price (large size)',         10000000, 22222, 0.25, 10.0, DEFAULT_LIQUIDATION_TIMESTAMP, 16667,  59998800023, 546, true ],
	[ 'should liquidate when oracle price is below price (large protection)',     100000, 22222, 0.95, 10.0, DEFAULT_LIQUIDATION_TIMESTAMP,  1111,   9000900090, 546, true ],
	[ 'should liquidate when oracle price is below price (small protection)',     100000, 22222, 0.05, 10.0, DEFAULT_LIQUIDATION_TIMESTAMP, 21111,    473686703, 546, true ],
];

// These test cases test maturation using some basic configurations
export const matureFixtures: ContractFixture[] =
[
	[ 'should not mature when message timestamp is before maturity timestamp', 100000, 22222, 0.25, 10.0, MATURITY_TIMESTAMP - 10, 22222,       546,       546, AuthenticationErrorCommon.failedVerify ],
	[ 'should mature when oracle timestamp is at maturity timestamp',          100000, 22222, 0.25, 10.0, MATURITY_TIMESTAMP,      22222, 450004500, 149983501, true ],
	[ 'should mature when oracle timestamp is after maturity timestamp',       100000, 22222, 0.25, 10.0, MATURITY_TIMESTAMP + 5,  22222, 450004500, 149983501, true ],
];
