import { UseCase } from './interfaces.js';

const useCase: UseCase =
{
	/*
	The data in this use case example represents a 1-week long hedging contract of $10.
	The initial data was derived from a functional example with the following on-chain activity:
	NOTE: The data was updated manually at different times because of changes to the library and contract
	*/

	contract:
	{
		createContract:
		{
			input:
			{
				takerSide: 'hedge',
				makerSide: 'long',
				oraclePublicKey: '029174c105b4d7be73b0e25b3b204dfab054bd2c12f7d7e38a5de4f4d05decc58f',
				hedgePayoutAddress: 'bitcoincash:qq59hv6s3qdjrtyfwfxxldkuj9xsjmx48vrz882knz',
				longPayoutAddress: 'bitcoincash:qpzlruwy4xu5rxjs3z37nsj29y7h59gwvsu4ddp0u4',
				nominalUnits: 1000,
				startingOracleMessage: 'db6409000100000001000000305c0000',
				startingOracleSignature: 'a8744a655a03107fb3b8e46eaee5519899960f258726d4fa6a74b6cbeb9a62efe96e012233cfbefc44378b820eb76bc4ef11e196ae5d47116ed9cbad93c6a818',
				maturityTimestamp: 616652,
				highLiquidationPriceMultiplier: 10,
				lowLiquidationPriceMultiplier: 0.75,
				enableMutualRedemption: 1,
				hedgeMutualRedeemPublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
				longMutualRedeemPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
			},
			output:
			{
				// TODO: Have to confirm new generated address is accurate
				address: 'bitcoincash:ppwz8zdfglkkvs73g5s3xr6qtf0vtsztwg96upa99h',
				parameters:
				{
					highLiquidationPrice: 236000,
					lowLiquidationPrice: 17700,
					startTimestamp: 615643,
					maturityTimestamp: 616652,
					oraclePublicKey: '029174c105b4d7be73b0e25b3b204dfab054bd2c12f7d7e38a5de4f4d05decc58f',
					hedgeLockScript: '76a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac',
					longLockScript: '76a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac',
					hedgeMutualRedeemPublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longMutualRedeemPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					nominalUnitsXSatsPerBch: 100_000_000_000,
					payoutSats: 5649718,
					enableMutualRedemption: 1,
				},
				metadata:
				{
					takerSide: 'hedge',
					makerSide: 'long',
					hedgePayoutAddress: 'bitcoincash:qq59hv6s3qdjrtyfwfxxldkuj9xsjmx48vrz882knz',
					longPayoutAddress: 'bitcoincash:qpzlruwy4xu5rxjs3z37nsj29y7h59gwvsu4ddp0u4',
					durationInSeconds: 1009,
					highLiquidationPriceMultiplier: 10,
					lowLiquidationPriceMultiplier: 0.75,
					startingOracleMessage: 'db6409000100000001000000305c0000',
					startingOracleSignature: 'a8744a655a03107fb3b8e46eaee5519899960f258726d4fa6a74b6cbeb9a62efe96e012233cfbefc44378b820eb76bc4ef11e196ae5d47116ed9cbad93c6a818',
					startPrice: 23600,
					nominalUnits: 1000,
					hedgeInputInSatoshis: 4237288,
					longInputInSatoshis: 1412430,

					// the start and maturity timestamps are small (e.g. 615643) compared to the other contracts (e.g. 1620000000).
					// This causes different minimal-encoding to take place, and the contract size changes:
					// for larger timestamps, bytesize=342 ==> minerCost=631 bytes
					// for smaller timestamps, bytesize=338 ==> minerCost=627
					minerCostInSatoshis: 627,
					hedgeInputInOracleUnits: 999.9999680000001,
					longInputInOracleUnits: 333.33348,
				},
				fees: [],
				fundings: [],
				version: 'AnyHedge v0.11',
			},
		},
		validateContract:
		{
			input:
			{
				// TODO: Have to confirm new generated address is accurate
				contractAddress: 'bitcoincash:ppwz8zdfglkkvs73g5s3xr6qtf0vtsztwg96upa99h',
				takerSide: 'hedge',
				makerSide: 'long',
				oraclePublicKey: '029174c105b4d7be73b0e25b3b204dfab054bd2c12f7d7e38a5de4f4d05decc58f',
				hedgePayoutAddress: 'bitcoincash:qq59hv6s3qdjrtyfwfxxldkuj9xsjmx48vrz882knz',
				longPayoutAddress: 'bitcoincash:qpzlruwy4xu5rxjs3z37nsj29y7h59gwvsu4ddp0u4',
				nominalUnits: 1000,
				startingOracleMessage: 'db6409000100000001000000305c0000',
				startingOracleSignature: 'a8744a655a03107fb3b8e46eaee5519899960f258726d4fa6a74b6cbeb9a62efe96e012233cfbefc44378b820eb76bc4ef11e196ae5d47116ed9cbad93c6a818',
				maturityTimestamp: 616652,
				highLiquidationPriceMultiplier: 10,
				lowLiquidationPriceMultiplier: 0.75,
				enableMutualRedemption: 1,
				hedgeMutualRedeemPublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
				longMutualRedeemPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
			},
			output: true,
		},
		addContractFee:
		{
			input:
			[
				// NOTE: adding a contract fee does not require the full contract data structure, but does require a list for the fees.
				{ fees: [] },
				{
					name: 'test',
					description: 'description of test a fee',
					address: 'bitcoincash:qraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
					satoshis: 10000,
				},
			],
			output:
			{
				fees:
				[
					{
						name: 'test',
						description: 'description of test a fee',
						address: 'bitcoincash:qraaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
						satoshis: 10000,
					},
				],
			},
		},
	},
	liquidation:
	{
		calculateMaturationOutcome:
		{
			input:
			[
				{
					highLiquidationPrice: 236000,
					lowLiquidationPrice: 17700,
					startTimestamp: 615643,
					maturityTimestamp: 616652,
					oraclePublicKey: '029174c105b4d7be73b0e25b3b204dfab054bd2c12f7d7e38a5de4f4d05decc58f',
					hedgeLockScript: '76a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac',
					longLockScript: '76a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac',
					hedgeMutualRedeemPublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longMutualRedeemPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					nominalUnitsXSatsPerBch: 100_000_000_000,
					payoutSats: 5649718,
				},
				// total funding sats
				5650263,
				// oracle price
				17500,
			],
			output:
			{
				hedgePayoutSatsSafe: 5649717,
				longPayoutSatsSafe: 546,
				totalPayoutSatsSafe: 5650263,
				minerFeeSats: 0,
			},
		},
	},
	maturation:
	{
		calculateMaturationOutcome:
		{
			input:
			[
				{
					highLiquidationPrice: 236000,
					lowLiquidationPrice: 17700,
					startTimestamp: 615643,
					maturityTimestamp: 616652,
					oraclePublicKey: '029174c105b4d7be73b0e25b3b204dfab054bd2c12f7d7e38a5de4f4d05decc58f',
					hedgeLockScript: '76a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac',
					longLockScript: '76a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac',
					hedgeMutualRedeemPublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longMutualRedeemPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					nominalUnitsXSatsPerBch: 100_000_000_000,
					payoutSats: 5649718,
				},
				// total funding sats
				5650263,
				// oracle price
				23500,
			],
			output:
			{
				hedgePayoutSatsSafe: 4255319,
				longPayoutSatsSafe: 1394399,
				totalPayoutSatsSafe: 5649718,
				minerFeeSats: 545,
			},
		},
	},
};

export default useCase;
