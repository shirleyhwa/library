import { UseCase } from './interfaces.js';
import hedge10week from './hedge10week.js';

export { UseCase };
export const useCases: { [index: string]: UseCase } =
{
	// Hedge 10 USD for one week.
	hedge10week,
};
