import type { Transaction } from 'cashscript';
import { AuthenticationProgramBCH, Output, hexToBin, binToHex, encodeTransaction, decodeTransactionUnsafe, Transaction as LibauthTransaction, createVirtualMachineBCH, decodeTransaction, summarizeDebugTrace, stringifyDebugTraceSummary, AuthenticationProgramStateCommon, encodeLockingBytecodeP2pkh, hash160 } from '@bitauth/libauth';
import { AnyHedgeManager, ContractData, ContractCreationParameters } from '../lib/index.js';
import { addressToLockScript } from '../lib/util/bitcoincash-util.js';
import { TAKER_SIDE, MAKER_SIDE, PROD_ORACLE_PUBKEY, DEFAULT_NOMINAL_UNITS, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER, DEFAULT_START_PRICE, HEDGE_ADDRESS, LONG_ADDRESS, HEDGE_PUBKEY, LONG_PUBKEY, ORACLE_PUBKEY, ORACLE_WIF, START_TIMESTAMP, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_DURATION } from './fixture/constants.js';

// Import utility function to handle oracle data.
import { OracleData } from '@generalprotocols/price-oracle';

// Create a fake version of the contract.broadcast() function that sets the transaction's
// locktime to a specific value and returns the built transaction
export type BroadcastFunction = (transactionBuilder: Transaction) => Promise<string>;
export const createFakeBroadcastWithFakeLocktime = function(locktime?: number): BroadcastFunction
{
	const fakeBroadcast = async (transactionBuilder: Transaction): Promise<string> =>
	{
		// If a custom locktime is provided we set it.
		if(locktime)
		{
			transactionBuilder.withTime(locktime);
		}

		// Build the transaction rather than broadcasting.
		return transactionBuilder.build();
	};

	return fakeBroadcast;
};

// Create a fake version of the bitcoincash-util::broadcastTransaction function that
// returns the transaction without broadcasting
export type BroadcastTransactionFunction = (transactionHex: string) => Promise<string>;
export const createFakeBroadcastTransaction = function(): BroadcastTransactionFunction
{
	const fakeBroadcast =
		async (transactionHex: string): Promise<string> => transactionHex;

	return fakeBroadcast;
};

// Create a libauth compatible source output to use in the Authentication Program for P2PKH
export const createSourceOutputP2PKH = function(
	publicKeyHex: string,
	satoshis: number,
): Output
{
	const publicKeyHashBin = hash160(hexToBin(publicKeyHex));
	const lockingBytecode = encodeLockingBytecodeP2pkh(publicKeyHashBin);
	const sourceOutput = { lockingBytecode, valueSatoshis: BigInt(satoshis) };

	return sourceOutput;
};

// Create a libauth compatible Authentication Program for P2PKH (for a single input with provided index) to be evaluated
export const createProgramP2PKH = function(
	transactionHex: string,
	publicKeyHex: string,
	satoshis: number,
	inputIndex: number = 0,
): AuthenticationProgramBCH
{
	const sourceOutputs = [];
	sourceOutputs[inputIndex] = createSourceOutputP2PKH(publicKeyHex, satoshis);
	const transaction = decodeTransactionUnsafe(hexToBin(transactionHex));
	const program = { inputIndex, sourceOutputs, transaction };

	return program;
};

// Create a libauth compatible source output to use in the Authentication Program
export const createSourceOutput = async function(
	manager: AnyHedgeManager,
	contractData: ContractData,
	satoshis: number,
): Promise<Output>
{
	const contractInstance = await manager.compileContract(contractData.parameters);
	const lockingBytecode = hexToBin(addressToLockScript(contractInstance.address));
	const sourceOutput = { lockingBytecode, valueSatoshis: BigInt(satoshis) };

	return sourceOutput;
};

// Create a libauth compatible Authentication Program (for a single input with provided index) to be evaluated
export const createProgram = async function(
	manager: AnyHedgeManager,
	contractData: ContractData,
	transactionHex: string,
	satoshis: number,
	inputIndex: number = 0,
): Promise<AuthenticationProgramBCH>
{
	const sourceOutputs = [];
	sourceOutputs[inputIndex] = await createSourceOutput(manager, contractData, satoshis);
	const transaction = decodeTransaction(hexToBin(transactionHex)) as LibauthTransaction;
	const program = { inputIndex, sourceOutputs, transaction };

	return program;
};

// Evaluate a libauth Authentication Program on a libauth VM
export const evaluateProgram = function(program: AuthenticationProgramBCH):
{ result: string | true; summary: string; trace: AuthenticationProgramStateCommon[] }
{
	const vm = createVirtualMachineBCH();
	const trace = vm.debug(program);
	const result = vm.stateSuccess(trace[trace.length - 1]);
	const summary = stringifyDebugTraceSummary(summarizeDebugTrace(trace));

	return { result, summary, trace };
};

// Test an `AuthenticationProgramBCH`, on failure, log a trace summary
export const programReturnsExpectedResult = (program: AuthenticationProgramBCH, expectedResult: string | true, programType?: string): void =>
{
	const { result, summary } = evaluateProgram(program);
	if(result !== expectedResult)
	{
		console.log(`Trace summary ${programType ? `for ${programType}` : ''}:`);
		console.log(summary);
	}
	expect(result).toEqual(expectedResult);
};

// Generates the meep command to debug a libauth Authentication Program
export const meepProgram = function(program: AuthenticationProgramBCH): string
{
	// Serialize the spending transaction from a Transaction object to a hex string
	const transactionHex = binToHex(encodeTransaction(program.transaction));

	// Extract the input index from the program
	const { inputIndex } = program;

	// Serialize the input's amount from a 64-bit little endian Uint8Array to a number
	const inputAmount = program.sourceOutputs[program.inputIndex].valueSatoshis;

	// Serialize the locking script from a Uint8Array to a hex string
	const inputLockingScript = binToHex(program.sourceOutputs[program.inputIndex].lockingBytecode);

	return `meep debug --tx=${transactionHex} --idx=${inputIndex} --amt=${inputAmount} --pkscript=${inputLockingScript}`;
};

// Create contract data filled with default fixture data
export const loadDefaultContractData = async function(
	manager: AnyHedgeManager,
	overrides: Partial<ContractCreationParameters> = {},
): Promise<ContractData>
{
	const VALID_ORACLE_MESSAGE = await OracleData.createPriceMessage(START_TIMESTAMP, 1, 1, DEFAULT_START_PRICE);
	const VALID_ORACLE_SIGNATURE = await OracleData.signMessage(VALID_ORACLE_MESSAGE, ORACLE_WIF);

	const defaultCreationParameters =
	{
		takerSide: TAKER_SIDE,
		makerSide: MAKER_SIDE,
		oraclePublicKey: ORACLE_PUBKEY,
		hedgePayoutAddress: HEDGE_ADDRESS,
		longPayoutAddress: LONG_ADDRESS,
		nominalUnits: DEFAULT_NOMINAL_UNITS,
		startingOracleMessage: binToHex(VALID_ORACLE_MESSAGE),
		startingOracleSignature: binToHex(VALID_ORACLE_SIGNATURE),
		maturityTimestamp: START_TIMESTAMP + DEFAULT_DURATION,
		highLiquidationPriceMultiplier: DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier: 1 - DEFAULT_VOLATILITY_PROTECTION,
		enableMutualRedemption: 1,
		hedgeMutualRedeemPublicKey: HEDGE_PUBKEY,
		longMutualRedeemPublicKey: LONG_PUBKEY,
	};

	const creationParameters = { ...defaultCreationParameters, ...overrides };

	// Create a contract using the default parameters.
	const contractData = await manager.createContract(creationParameters);

	return contractData;
};

// Register a contract with default fixture data (and a random start timestamp)
export const registerDefaultContractData = async function(
	manager: AnyHedgeManager,
): Promise<ContractData>
{
	// Generate a random start timestamp (so every new contract is unique)
	const startTimestamp = Math.round(Math.random() * 100000000);

	const VALID_ORACLE_MESSAGE = await OracleData.createPriceMessage(startTimestamp, 1, 1, DEFAULT_START_PRICE);
	const VALID_ORACLE_SIGNATURE = await OracleData.signMessage(VALID_ORACLE_MESSAGE, ORACLE_WIF);

	const registrationParameters =
	{
		takerSide: TAKER_SIDE,
		makerSide: MAKER_SIDE,
		oraclePublicKey: PROD_ORACLE_PUBKEY,
		hedgePayoutAddress: HEDGE_ADDRESS,
		longPayoutAddress: LONG_ADDRESS,
		nominalUnits: DEFAULT_NOMINAL_UNITS,
		startingOracleMessage: binToHex(VALID_ORACLE_MESSAGE),
		startingOracleSignature: binToHex(VALID_ORACLE_SIGNATURE),
		maturityTimestamp: startTimestamp + DEFAULT_DURATION,
		highLiquidationPriceMultiplier: DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier: 1 - DEFAULT_VOLATILITY_PROTECTION,
		enableMutualRedemption: 1,
		hedgeMutualRedeemPublicKey: HEDGE_PUBKEY,
		longMutualRedeemPublicKey: LONG_PUBKEY,
	};

	// Register a contract using the default parameters.
	const contractData = await manager.registerContractForSettlement(registrationParameters);

	return contractData;
};
