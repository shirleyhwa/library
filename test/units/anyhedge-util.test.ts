/* eslint-disable max-nested-callbacks */
import { binToHex, encodeTransaction, hexToBin } from '@bitauth/libauth';
import { isParsedMutualRedemptionData, isParsedPayoutData, SettlementType, AnyHedgeManager, SettlementParseError } from '../../lib/index.js';
import { estimatePayoutTransactionFee, parseSettlementTransaction } from '../../lib/util/anyhedge-util.js';
import { loadDefaultContractData } from '../test-util.js';

describe('(estimatePayoutTransactionFee()', () =>
{
	test('should estimate payout transaction size', async () =>
	{
		// Set up default test data.
		const manager = new AnyHedgeManager();
		const contractData = await loadDefaultContractData(manager);

		// Compile the contract for the default contract data.
		const contract = await manager.compileContract(contractData.parameters);

		// Estimate the payout transaction size.
		// Check that the payout transaction size matches the expectations
		// TODO: This is an integration test that is hard to verify. Need for improvement noted in #267
		await expect(estimatePayoutTransactionFee(contract, contractData.parameters, 1.0)).resolves.toEqual(631);
	});
});

describe('parseSettlementTransaction()', () =>
{
	// Retrieved from an actual payout transaction of a test contract.
	// https://blockchair.com/bitcoin-cash/transaction/7b5a36707fa5319ff0de6d7544da024f14aa146cbd9cf3c930d059e4e172e977
	const payoutTransaction =
	{
		inputs:
		[
			{
				outpointIndex: 0,
				outpointTransactionHash: hexToBin('9dc54b5580e65a9a7ca36b698b54b210e427105dd796ad24a8db60c7545010b3'),
				sequenceNumber: 4294967294,
				unlockingBytecode: hexToBin('4007e90abb5e4baf74b15537ef63428a6226d815648bedd807c6ef6561b097a6cbdc773044fbe264749cc5b493ba88aa069ffa422ec150d20a05d8061c8e0254ed10b097a4628f3e01006a3e0100d63e000040754d256b0266f80bae0016704944fc5ef28aa7dfe37035c29ddbbe08b6a68297d13bc6b676d514d58ba07f4affa6d3f834f7d8fea1b5bd1b4f4e132ffbb35cbc10ec97a462903e01006b3e0100d03e0000514d540104ec97a46204c096a4620380730202102f031f55060500f2052a012102d3c1de9d4bc77d6c3608cbe44d10138c7488e592dc2b1e10a6cf0e92c2ecb0471976a91415d54e1b90d806548f34263afe71695a8a19716388ac1976a914ec4bebcc7842bdc802880e8692d83e9ec41b95d688ac51210374059eb4b0edb9052779a1ef93c76d1715473a9f3d6634135a3c03b82561a015210396dc6749e3bde2c230fb10bb66a444d83318521c449181a6be57123c1257a6575c79009c637b695c7a7cad5b7a7cad6d6d6d6d6d51675c7a519dc3519d5f7a5f795779bb5d7a5d79577abb5c79587f77547f75817600a0695c79587f77547f75818c9d5c7a547f75815b799f695b795c7f77817600a0695979a35879a45c7a547f7581765c7aa2695b7aa2785a7a8b5b7aa5919b6902220276587a537a96a47c577a527994a4c4529d00cc7b9d00cd557a8851cc9d51cd547a8777777768'),
			},
		],
		locktime: 0,
		outputs:
		[
			{
				lockingBytecode: hexToBin('76a914ec4bebcc7842bdc802880e8692d83e9ec41b95d688ac'),
				valueSatoshis: BigInt(310945),
			},
			{
				lockingBytecode: hexToBin('76a91415d54e1b90d806548f34263afe71695a8a19716388ac'),
				valueSatoshis: BigInt(104062),
			},
		],
		version: 2,
	};
	// Retrieved from an actual mutual redemption transaction of a test contract.
	// https://blockchair.com/bitcoin-cash/transaction/00691d69cbf0359c1f8ddda2c3897b0a43edf53afa618eef5d1ccde7edf5b825
	const mutualRedemptionTransaction =
	{
		inputs:
		[
			{
				outpointIndex: 0,
				outpointTransactionHash: hexToBin('db9775d94f8984fbaedfdca416690065647b079c8a9f454d1abcba1d99a1954c'),
				sequenceNumber: 0,
				unlockingBytecode: hexToBin('4158787bb408d55e1ce0c24f6fa8fa817b79c2e60b65679a67dab13abf599998bd61d3b70ae70b22a77044310c6189ebef3ea8df9177ecce31d3be4068d6295e6741415557d1f27e9a37a702f60f74f3fa7ebd7f3122e40d0aacac41681a766ae79d3b6fd486c8e033d1ad3569011786fcb63c4fd9b3bf61486ccbb36ce76a316c405c41004d5401044fb20863044f69ac620332b30102a420036521090500f2052a012102d3c1de9d4bc77d6c3608cbe44d10138c7488e592dc2b1e10a6cf0e92c2ecb0471976a91415d54e1b90d806548f34263afe71695a8a19716388ac1976a914ec4bebcc7842bdc802880e8692d83e9ec41b95d688ac51210374059eb4b0edb9052779a1ef93c76d1715473a9f3d6634135a3c03b82561a015210396dc6749e3bde2c230fb10bb66a444d83318521c449181a6be57123c1257a6575c79009c637b695c7a7cad5b7a7cad6d6d6d6d6d51675c7a519dc3519d5f7a5f795779bb5d7a5d79577abb5c79587f77547f75817600a0695c79587f77547f75818c9d5c7a547f75815b799f695b795c7f77817600a0695979a35879a45c7a547f7581765c7aa2695b7aa2785a7a8b5b7aa5919b6902220276587a537a96a47c577a527994a4c4529d00cc7b9d00cd557a8851cc9d51cd547a8777777768'),
			},
		],
		locktime: 0,
		outputs:
		[
			{
				lockingBytecode: hexToBin('76a914ec4bebcc7842bdc802880e8692d83e9ec41b95d688ac'),
				valueSatoshis: BigInt(448793),
			},
			{
				lockingBytecode: hexToBin('76a91415d54e1b90d806548f34263afe71695a8a19716388ac'),
				valueSatoshis: BigInt(149580),
			},
		],
		version: 2,
	};

	test('should throw when transaction contains more than one input', async () =>
	{
		// Duplicate the inputs, so that there are 2 inputs rather than 1.
		const transaction = { ...payoutTransaction, inputs: [ ...payoutTransaction.inputs, ...payoutTransaction.inputs ] };

		// Encode the transaction.
		const encodedTransaction = binToHex(encodeTransaction(transaction));

		// Attempt to parse the settlement transaction.
		// Check that the call throws a SettlementParseError.
		await expect(() => parseSettlementTransaction(encodedTransaction)).rejects.toThrowError(SettlementParseError);
	});

	test('should throw when transaction does not contain two outputs', async () =>
	{
		// Only use the first output, so that there is 1 output rather than 2.
		const outputs = [ payoutTransaction.outputs[0] ];
		const transaction = { ...payoutTransaction, outputs };

		// Encode the transaction.
		const encodedTransaction = binToHex(encodeTransaction(transaction));

		// Attempt to parse the settlement transaction.
		// Check that the call throws a SettlementParseError.
		await expect(() => parseSettlementTransaction(encodedTransaction)).rejects.toThrowError(SettlementParseError);
	});

	test('should throw when transaction does not contain expected input parameters', async () =>
	{
		// Remove the unlocking bytecode from the transaction input so it does not contain any input parameters.
		const inputs = [{ ...payoutTransaction.inputs[0], unlockingBytecode: hexToBin('') }];
		const transaction = { ...payoutTransaction, inputs };

		// Encode the transaction.
		const encodedTransaction = binToHex(encodeTransaction(transaction));

		// Attempt to parse the settlement transaction.
		// Check that the call throws a SettlementParseError.
		await expect(() => parseSettlementTransaction(encodedTransaction)).rejects.toThrowError(SettlementParseError);
	});

	// re-enable with updated transaction content
	test('should successfully parse a mutual redemption transaction', async () =>
	{
		// Make a copy of the settlement transaction.
		const transaction = { ...mutualRedemptionTransaction };

		// Encode the transaction.
		const encodedTransaction = binToHex(encodeTransaction(transaction));

		// Parse the settlement transaction.
		const parsedSettlementData = await parseSettlementTransaction(encodedTransaction);

		// Define the expected data.
		const expectedSettlementData =
		{
			address: 'bitcoincash:ppm5jfz7f2hkvxq7rzcy23wye0qprcq20unt8zyd8r',
			funding:
			{
				fundingTransactionHash: 'db9775d94f8984fbaedfdca416690065647b079c8a9f454d1abcba1d99a1954c',
				fundingOutputIndex: 0,
			},
			parameters:
			{
				lowLiquidationPrice: 8356,
				highLiquidationPrice: 111410,
				startTimestamp: 1655466319,
				maturityTimestamp: 1661514319,
				oraclePublicKey: '02d3c1de9d4bc77d6c3608cbe44d10138c7488e592dc2b1e10a6cf0e92c2ecb047',
				hedgeLockScript: '76a914ec4bebcc7842bdc802880e8692d83e9ec41b95d688ac',
				longLockScript: '76a91415d54e1b90d806548f34263afe71695a8a19716388ac',
				hedgeMutualRedeemPublicKey: '0396dc6749e3bde2c230fb10bb66a444d83318521c449181a6be57123c1257a657',
				longMutualRedeemPublicKey: '0374059eb4b0edb9052779a1ef93c76d1715473a9f3d6634135a3c03b82561a015',
				enableMutualRedemption: 1,
				nominalUnitsXSatsPerBch: 5000000000,
				payoutSats: 598373,
			},
			settlement:
			{
				settlementType: SettlementType.MUTUAL,
				settlementTransactionHash: '00691d69cbf0359c1f8ddda2c3897b0a43edf53afa618eef5d1ccde7edf5b825',
				hedgePayoutInSatoshis: 448793,
				longPayoutInSatoshis: 149580,
			},
		};

		// Check that the actual data matches the expected data.
		expect(isParsedMutualRedemptionData(parsedSettlementData)).toBeTruthy();
		expect(parsedSettlementData).toEqual(expectedSettlementData);
	});

	// re-enable with updated transaction content
	test('should successfully parse a payout transaction', async () =>
	{
		// Make a copy of the settlement transaction.
		const transaction = { ...payoutTransaction };

		// Encode the transaction.
		const encodedTransaction = binToHex(encodeTransaction(transaction));

		// Parse the settlement transaction.
		const parsedSettlementData = await parseSettlementTransaction(encodedTransaction);

		// Define the expected data.
		const expectedSettlementData =
		{
			address: 'bitcoincash:pq27meje7y4v99tuyypvd37lcyf68jm83guwcyfy9l',
			funding:
			{
				fundingTransactionHash: '9dc54b5580e65a9a7ca36b698b54b210e427105dd796ad24a8db60c7545010b3',
				fundingOutputIndex: 0,
			},
			parameters:
			{
				lowLiquidationPrice: 12048,
				highLiquidationPrice: 160640,
				startTimestamp: 1654953664,
				maturityTimestamp: 1654953964,
				oraclePublicKey: '02d3c1de9d4bc77d6c3608cbe44d10138c7488e592dc2b1e10a6cf0e92c2ecb047',
				hedgeLockScript: '76a914ec4bebcc7842bdc802880e8692d83e9ec41b95d688ac',
				longLockScript: '76a91415d54e1b90d806548f34263afe71695a8a19716388ac',
				hedgeMutualRedeemPublicKey: '0396dc6749e3bde2c230fb10bb66a444d83318521c449181a6be57123c1257a657',
				longMutualRedeemPublicKey: '0374059eb4b0edb9052779a1ef93c76d1715473a9f3d6634135a3c03b82561a015',
				enableMutualRedemption: 1,
				nominalUnitsXSatsPerBch: 5000000000,
				payoutSats: 415007,
			},
			settlement:
			{
				settlementType: SettlementType.MATURATION,
				settlementTransactionHash: '7b5a36707fa5319ff0de6d7544da024f14aa146cbd9cf3c930d059e4e172e977',
				hedgePayoutInSatoshis: 310945,
				longPayoutInSatoshis: 104062,
				oraclePublicKey: '02d3c1de9d4bc77d6c3608cbe44d10138c7488e592dc2b1e10a6cf0e92c2ecb047',
				settlementMessage: 'ec97a462903e01006b3e0100d03e0000',
				settlementSignature: '754d256b0266f80bae0016704944fc5ef28aa7dfe37035c29ddbbe08b6a68297d13bc6b676d514d58ba07f4affa6d3f834f7d8fea1b5bd1b4f4e132ffbb35cbc',
				previousMessage: 'b097a4628f3e01006a3e0100d63e0000',
				previousSignature: '07e90abb5e4baf74b15537ef63428a6226d815648bedd807c6ef6561b097a6cbdc773044fbe264749cc5b493ba88aa069ffa422ec150d20a05d8061c8e0254ed',
				settlementPrice: 16080,
			},
		};

		// Check that the actual data matches the expected data.
		expect(isParsedPayoutData(parsedSettlementData)).toBeTruthy();
		expect(parsedSettlementData).toEqual(expectedSettlementData);
	});

	// For the following tests we need to hand-craft a P2SH transaction that looks like an AnyHedge transaction, but isn't.
	// This is currently not worth the effort as long as the function works correctly for *actual* AnyHedge transactions.
	test.todo('should throw when mutual redemption transaction redeem script does not match AnyHedge');
	test.todo('should throw when payout transaction redeem script does not match AnyHedge');
});
