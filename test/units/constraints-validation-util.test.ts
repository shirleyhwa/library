/* eslint-disable prefer-regex-literals */
/* eslint-disable line-comment-position */
import { AnyHedgeManager, ContractSide } from '../../lib/index.js';
import { SATS_PER_BCH, MIN_INTEGER_DIVISION_PRECISION_STEPS, MIN_LIQUIDATION_RELATIVE_PRICE_CHANGE, DUST_LIMIT, MAX_CONTRACT_SATS, JAVASCRIPT_FRIENDLY_SCRIPT_INT_MAX } from '../../lib/constants.js';
import { LowLiquidationPriceError, HighLiquidationPriceError, NominalUnitsXSatsPerBchError, PayoutSatsError } from '../../lib/errors.js';
import { validateIntegerDivisionPrecision } from '../../lib/util/constraints-validation-util.js';

// Import utility function to handle oracle data.
import { OracleData } from '@generalprotocols/price-oracle';

// Import utilities to handle hex encoded values.
import { binToHex } from '@bitauth/libauth';

import { ORACLE_PUBKEY, ORACLE_WIF } from '../fixture/constants.js';

// Note: Throughout the tests, many values have been determined as valid through empirical experimentation.
//       For example, when a value is increased (*1.1) or decreased (*0.99) by a bit in line with the test's intent,
//       the specific adjustment is determined by experimentation until it doesn't trigger tangent constraint violations.
//       When a value is determined analytically, the test will include a specific explanation.

// Sane default contract parameters
const VALID_TAKER_SIDE: ContractSide = 'hedge';
const VALID_MAKER_SIDE: ContractSide = 'long';
const VALID_TIMESTAMP = 1620000000;
const VALID_DURATION = 100;
const VALID_PAYOUT_ADDRESS = 'bitcoincash:qpzlruwy4xu5rxjs3z37nsj29y7h59gwvsu4ddp0u4';
const VALID_NOMINAL_UNITS = 10000;
const VALID_START_PRICE = 1000;
const VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER = 1.0 + (100 * MIN_LIQUIDATION_RELATIVE_PRICE_CHANGE);
const VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER = 1.0 - (10 * MIN_LIQUIDATION_RELATIVE_PRICE_CHANGE);
const VALID_ENABLE_MUTUAL_REDEMPTION_FLAG = 1;

// Note: See constraints-validation-util.ts for additional details on all contract creation boundary constraints

// Create an instance of the contract manager for access to createContract()
const contractManager = new AnyHedgeManager();

// Test fixture interface for feeding createContract()
interface ContractCreationTestFixture
{
	description: string;
	nominalUnits: number;
	startPrice: number;
	highLiquidationPriceMultiplier: number;
	lowLiquidationPriceMultiplier: number;
	expectedErrorType: any;
	expectedErrorMessageOrRegExp: (RegExp | string | null);
}

// Runs a contract creation test according to the input fixture specifications
const runContractCreationConstraintTest = async function(fixture: ContractCreationTestFixture): Promise<void>
{
	const VALID_ORACLE_MESSAGE = await OracleData.createPriceMessage(VALID_TIMESTAMP, 1, 1, Math.round(fixture.startPrice));
	const VALID_ORACLE_SIGNATURE = await OracleData.signMessage(VALID_ORACLE_MESSAGE, ORACLE_WIF);

	const creationParameters =
	{
		takerSide: VALID_TAKER_SIDE,
		makerSide: VALID_MAKER_SIDE,
		oraclePublicKey: ORACLE_PUBKEY,
		hedgePayoutAddress: VALID_PAYOUT_ADDRESS,
		longPayoutAddress: VALID_PAYOUT_ADDRESS,
		nominalUnits: fixture.nominalUnits,
		startingOracleMessage: binToHex(VALID_ORACLE_MESSAGE),
		startingOracleSignature: binToHex(VALID_ORACLE_SIGNATURE),
		maturityTimestamp: VALID_TIMESTAMP + VALID_DURATION,
		highLiquidationPriceMultiplier: fixture.highLiquidationPriceMultiplier,
		lowLiquidationPriceMultiplier: fixture.lowLiquidationPriceMultiplier,
		enableMutualRedemption: VALID_ENABLE_MUTUAL_REDEMPTION_FLAG,
		hedgeMutualRedeemPublicKey: ORACLE_PUBKEY,
		longMutualRedeemPublicKey: ORACLE_PUBKEY,
	};

	// Set up a function to create the contract repeatably in various contexts
	const test = async function(): Promise<void>
	{
		await contractManager.createContract(creationParameters);
	};

	// Confirm success or error as specified by the fixture
	if(fixture.expectedErrorType === null)
	{
		// Confirm that the creation works fine when no error is expected
		await expect(test()).resolves.not.toThrow();
	}
	else
	{
		// Confirm the error type
		await expect(test()).rejects.toThrowError(fixture.expectedErrorType);

		// Confirm the error message
		if(fixture.expectedErrorMessageOrRegExp !== null)
		{
			await expect(test()).rejects.toThrowError(fixture.expectedErrorMessageOrRegExp);
		}
	}
};

// Reasonable happy path contract should pass
const happyPathFixture: ContractCreationTestFixture = {
	description:                    'should succeed with reasonable values within all safety and policy ranges',
	nominalUnits:                   VALID_NOMINAL_UNITS,
	startPrice:                     VALID_START_PRICE,
	highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
	lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
	expectedErrorType:              null,
	expectedErrorMessageOrRegExp:   null,
};
await runContractCreationConstraintTest(happyPathFixture);

/*
	High Liquidation Price Multiplier Validation Tests
*/

// Fixtures that confirm contracts behave correctly at high liquidation price boundaries. Start high and go down.
const highLiquidationPriceTestFixtures: ContractCreationTestFixture[] =
[
	// Note: Fractional values used in these tests, e.g. 0.9, are applied to *floating point intent* parameters which
	//       get translated through library code paths into the *integer contract parameters* that we are testing.

	// Note: the validation code describes why a test for maximum is not needed and therefore tests start with minimum.

	// Note: a test for high liquidation price above the minimum set by policy is tested by the happy path above
	//     -----------------|startPrice --------------|policyMinimum ----------->
	//                                                      ^ highLiquidationPrice

	{
		// -----------------|startPrice --------------|policyMinimum ----------->
		//                                            ^ highLiquidationPrice
		description:                    'should succeed with high liquidation price at the minimum policy boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: 1.0 + MIN_LIQUIDATION_RELATIVE_PRICE_CHANGE,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		expectedErrorType:              null,
		expectedErrorMessageOrRegExp:   null,
	},
	{
		// -----------------|startPrice --------------|policyMinimum ----------->
		//                                   ^ highLiquidationPrice
		description:                    'should fail with high liquidation price greater than start price safety boundary, but less than the minimum policy boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: 1.0 + (MIN_LIQUIDATION_RELATIVE_PRICE_CHANGE / 2),
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		expectedErrorType:              HighLiquidationPriceError,
		expectedErrorMessageOrRegExp:   new RegExp('High liquidation price must be sufficiently greater than start price'),
	},
	{
		// -----------------|startPrice --------------|policyMinimum ----------->
		//                  ^ highLiquidationPrice
		description:                    'should fail more specifically than policy with high liquidation price at the start price safety boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: 1.0,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		expectedErrorType:              HighLiquidationPriceError,
		expectedErrorMessageOrRegExp:   new RegExp('High liquidation price must be greater than ceiling of start price'),
	},
	{
		// -----------------|startPrice --------------|policyMinimum ----------->
		//           ^ highLiquidationPrice
		description:                    'should fail more specifically than policy with high liquidation price under the start price safety boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: 0.9,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		expectedErrorType:              HighLiquidationPriceError,
		expectedErrorMessageOrRegExp:   new RegExp('High liquidation price must be greater than ceiling of start price'),
	},
];
test.each(highLiquidationPriceTestFixtures)('%s', runContractCreationConstraintTest);

/*
	Low Liquidation Price Multiplier Validation Tests
*/

// We need the minimum boundary for low liquidation price, which is simply the integer 1 (0 has undefined behavior).
// We are starting from intent parameters, so we need to reverse-calculate the intent that results in the desired contract value.
const lowLiquidationMultiplierForLowLiquidationPriceOfOne = 1 / VALID_START_PRICE;

// Fixtures that confirm contracts behave correctly at low liquidation price boundaries. Start high and go down.
const lowLiquidationPriceTestFixtures: ContractCreationTestFixture[] =
[
	// Note: Fractional values used in these tests, e.g. 1.1, are applied to *floating point intent* parameters which
	//       get translated through library code paths into the *integer contract parameters* that we are testing.

	{
		// ----|0|1-------------|policyMaximum --------------|startPrice ----------->
		//                                                          ^ lowLiquidationPrice
		description:                    'should fail more specifically than policy with low liquidation price over the start price safety boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  1.1,
		expectedErrorType:              LowLiquidationPriceError,
		expectedErrorMessageOrRegExp:   new RegExp('Low liquidation price must be less than floor of start price'),
	},
	{
		// ----|0|1-------------|policyMaximum --------------|startPrice ----------->
		//                                                   ^ lowLiquidationPrice
		description:                    'should fail more specifically than policy with low liquidation price at the start price safety boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  1.0,
		expectedErrorType:              LowLiquidationPriceError,
		expectedErrorMessageOrRegExp:   new RegExp('Low liquidation price must be less than floor of start price'),
	},
	{
		// ----|0|1-------------|policyMaximum --------------|startPrice ----------->
		//                                   ^ lowLiquidationPrice
		description:                    'should fail with low liquidation price less than the start price safety boundary, but greater than the maximum policy boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  1.0 - (MIN_LIQUIDATION_RELATIVE_PRICE_CHANGE / 2),
		expectedErrorType:              LowLiquidationPriceError,
		expectedErrorMessageOrRegExp:   new RegExp('Low liquidation price must be sufficiently less than start price'),
	},
	{
		// ----|0|1-------------|policyMaximum --------------|startPrice ----------->
		//                      ^ lowLiquidationPrice
		description:                    'should succeed with low liquidation price at the maximum policy boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  1.0 - MIN_LIQUIDATION_RELATIVE_PRICE_CHANGE,
		expectedErrorType:              null,
		expectedErrorMessageOrRegExp:   null,
	},

	// Note: a test for low liquidation price between the lower safety boundary and upper policy boundary is tested by the happy path above
	//     ----|0|1-------------|policyMaximum --------------|startPrice ----------->
	//                 ^ lowLiquidationPrice

	{
		// ----|0|1-------------|policyMaximum --------------|startPrice ----------->
		//       ^ lowLiquidationPrice
		description:                    'should succeed with low liquidation price at the 1 safety boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  lowLiquidationMultiplierForLowLiquidationPriceOfOne,
		expectedErrorType:              null,
		expectedErrorMessageOrRegExp:   null,
	},
	{
		// ----|0|1-------------|policyMaximum --------------|startPrice ----------->
		//     ^ lowLiquidationPrice
		description:                    'should fail with low liquidation price below the 1 safety boundary',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  0.0,
		expectedErrorType:              LowLiquidationPriceError,
		expectedErrorMessageOrRegExp:   new RegExp('Low liquidation price must be at least 1 but got 0'),
	},
];
test.each(lowLiquidationPriceTestFixtures)('%s', runContractCreationConstraintTest);

/*
	Nominal Units Validation Tests
*/

// Given a specific nominal units value, we need a high liquidation price that results in division precision equal to the minimum precision boundary.
// We are starting from intent parameters, so we need to reverse-calculate the intent that results in the desired contract value.
const highLiquidationPriceAtDivisionPrecisionBoundary = VALID_NOMINAL_UNITS * (SATS_PER_BCH / MIN_INTEGER_DIVISION_PRECISION_STEPS);
const highLiquidationPriceMultiplierAtDivisionPrecisionBoundary = highLiquidationPriceAtDivisionPrecisionBoundary / VALID_START_PRICE;

// We need a compound nominal unit value equal to its upper boundary condition (valid script integer).
// We are starting from intent parameters, so we need to reverse-calculate the intent that results in the desired contract value.
const nominalUnitsAtScriptIntBoundary = JAVASCRIPT_FRIENDLY_SCRIPT_INT_MAX / SATS_PER_BCH;

// If the start price is too small, the satoshis required for payout will overflow some tangent constraints.
// Therefore, in some tests where we are working with large payouts, we have experimentally determined
// a high start price that does not trigger other constraints.
const higherStartPriceViableForPayoutAtScriptIntBoundary = 10 * VALID_START_PRICE;

// Fixtures that confirm contracts behave correctly at compound nominal value boundaries
const compoundNominalValueConstraintsTestFixtures: ContractCreationTestFixture[] =
[
	// Note: Fractional values used in these tests, e.g. 1.1, are applied to *floating point intent* parameters which
	//       get translated through library code paths into the *integer contract parameters* that we are testing.

	{
		// Not showing a number line since this is a special case maximum.
		description:                    'should fail with high liquidation price too high/close to compound nominal value, which limits calculation precision',
		nominalUnits:                   VALID_NOMINAL_UNITS,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: highLiquidationPriceMultiplierAtDivisionPrecisionBoundary,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		expectedErrorType:              NominalUnitsXSatsPerBchError,
		expectedErrorMessageOrRegExp:   new RegExp('Regarding the division between compound nominal value and high liquidation price'),
	},
	{
		// ------------|1 ---------------|maxScriptInt ----------->
		//                                       ^ nominalUnitsXSatsPerBch
		description:                    'should fail with nominal units greater than maximum script integer safety boundary',
		nominalUnits:                   1.1 * nominalUnitsAtScriptIntBoundary,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		expectedErrorType:              NominalUnitsXSatsPerBchError,
		expectedErrorMessageOrRegExp:   new RegExp('compound nominal value must be at most the maximum allowed script integer'),
	},
	{
		// ------------|1 ---------------|maxScriptInt ----------->
		//                               ^ nominalUnitsXSatsPerBch
		description:                    'should succeed with nominal units at maximum script integer safety boundary',
		nominalUnits:                   nominalUnitsAtScriptIntBoundary,
		startPrice:                     higherStartPriceViableForPayoutAtScriptIntBoundary,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		expectedErrorType:              null,
		expectedErrorMessageOrRegExp:   null,
	},

	// Note: a test for nominalUnitsXSatsPerBch between the safety boundaries is tested by the happy path above
	//     ------------|1 ---------------|maxScriptInt ----------->
	//                        ^ nominalUnitsXSatsPerBch

	{
		// ------------|1 ---------------|maxScriptInt ----------->
		//             ^ nominalUnitsXSatsPerBch
		description:                    'should succeed with nominal units at the 1 safety boundary',
		nominalUnits:                   1.0,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		expectedErrorType:              null,
		expectedErrorMessageOrRegExp:   null,
	},
	{
		// ------------|1 ---------------|maxScriptInt ----------->
		//        ^ nominalUnitsXSatsPerBch
		description:                    'should fail with nominal units less than the 1 safety boundary',
		nominalUnits:                   0.9,
		startPrice:                     VALID_START_PRICE,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		expectedErrorType:              NominalUnitsXSatsPerBchError,
		expectedErrorMessageOrRegExp:   new RegExp('nominal units must be at least 1'),
	},
];
test.each(compoundNominalValueConstraintsTestFixtures)('%s', runContractCreationConstraintTest);

/*
	Payout Satoshis Validation Tests
*/

// We need payout sats to be the maximum number possible, MAX_CONTRACT_SATS.
// Payout sats is effectively determined by the payout needed to cover hedge value at low liquidation.
// Therefore, we choose an arbitrary, small, low liquidation price and then calculate the nominal units that will
// result in payout sats equal to the maximum boundary.
const lowLiquidationPriceToInflatePayout = 100;
const nominalUnitsAtMaxPayout = (MAX_CONTRACT_SATS / SATS_PER_BCH) * lowLiquidationPriceToInflatePayout;
const startPriceDerivedFromInflationaryLowLiquidationPrice = lowLiquidationPriceToInflatePayout / VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER;

// Do a similar reverse calculation at the other extreme - the boundary where payout is only dust
const lowLiquidationPriceToDeflatePayout = 1_000_000_000;
const nominalUnitsAtDustPayout = DUST_LIMIT * (lowLiquidationPriceToDeflatePayout / SATS_PER_BCH);
const highStartPriceToDeflatePayoutSats = lowLiquidationPriceToDeflatePayout / VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER;

// Getting a reasonable high liquidation multiplier requires a little bit of careful calculation given the extreme high prices for dust payout
const highLiquidationPriceAtPrecisionBoundaryGivenDustPayout = nominalUnitsAtDustPayout * (SATS_PER_BCH / MIN_INTEGER_DIVISION_PRECISION_STEPS);
const highLiquidationMultiplierAtPrecisionBoundaryGivenDustPayout = highLiquidationPriceAtPrecisionBoundaryGivenDustPayout / highStartPriceToDeflatePayoutSats;
const highLiquidationMultiplierWithGoodEnoughPrecisionAtDustPayout = (1.0 + highLiquidationMultiplierAtPrecisionBoundaryGivenDustPayout) / 2;

// Fixtures that confirm contracts behave correctly at payout satoshis boundaries
const payoutSatsConstraintsTestFixtures: ContractCreationTestFixture[] =
[
	// Note: Fractional values used in these tests, e.g. 1.1, are applied to *floating point intent* parameters which
	//       get translated through library code paths into the *integer contract parameters* that we are testing.

	{
		// ------------|DUST ---------------|maxContractSats ----------->
		//                                           ^ payoutSats
		description:                    'should fail with payout over maximum allowed contract satoshis safety boundary',
		nominalUnits:                   1.1 * nominalUnitsAtMaxPayout,
		startPrice:                     startPriceDerivedFromInflationaryLowLiquidationPrice,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		expectedErrorType:              PayoutSatsError,
		expectedErrorMessageOrRegExp:   'Payout satoshis must be at most the maximum allowed contract satoshis safety boundary',
	},
	{
		// ------------|DUST ---------------|maxContractSats ----------->
		//                                  ^ payoutSats
		description:                    'should succeed with payout at maximum allowed contract satoshis safety boundary',
		nominalUnits:                   nominalUnitsAtMaxPayout,
		startPrice:                     startPriceDerivedFromInflationaryLowLiquidationPrice,
		highLiquidationPriceMultiplier: VALID_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		expectedErrorType:              null,
		expectedErrorMessageOrRegExp:   null,
	},

	// Note: a test for payoutSats between the safety boundaries is tested by the happy path above
	//      ------------|DUST ---------------|maxContractSats ----------->
	//                            ^ payoutSats

	{
		// ------------|DUST ---------------|maxContractSats ----------->
		//             ^ payoutSats
		description:                    'should fail with payout at dust limit safety boundary',
		nominalUnits:                   nominalUnitsAtDustPayout,
		startPrice:                     highStartPriceToDeflatePayoutSats,
		highLiquidationPriceMultiplier: highLiquidationMultiplierWithGoodEnoughPrecisionAtDustPayout,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		expectedErrorType:              PayoutSatsError,
		expectedErrorMessageOrRegExp:   'Payout satoshis must be greater than the dust limit',
	},
	{
		// ------------|DUST ---------------|maxContractSats ----------->
		//        ^ payoutSats
		description:                    'should fail with payout less than dust limit safety boundary',
		nominalUnits:                   0.99 * nominalUnitsAtDustPayout,
		startPrice:                     highStartPriceToDeflatePayoutSats,
		highLiquidationPriceMultiplier: highLiquidationMultiplierWithGoodEnoughPrecisionAtDustPayout,
		lowLiquidationPriceMultiplier:  VALID_LOW_LIQUIDATION_PRICE_MULTIPLIER,
		expectedErrorType:              PayoutSatsError,
		expectedErrorMessageOrRegExp:   'Payout satoshis must be greater than the dust limit',
	},
];
test.each(payoutSatsConstraintsTestFixtures)('%s', runContractCreationConstraintTest);

/*
	Division Precision Validation Tests
*/

// Confirm constraints enforced by division precision, which is used as a part of several higher level validators.
describe('validateIntegerDivisionPrecision()', () =>
{
	test('should pass when there are at least as many required division steps between numerator and denominator', async () =>
	{
		// Confirm integer behavior
		await expect(validateIntegerDivisionPrecision(5000, 2, 2500))
			.resolves.not.toThrow();
	});
	test('should fail if numerator is not a positive integer', async () =>
	{
		await expect(validateIntegerDivisionPrecision(1.2, 1, 1))
			.rejects.toThrowError('Numerator must be a positive integer');
		await expect(validateIntegerDivisionPrecision(0, 1, 1))
			.rejects.toThrowError('Numerator must be a positive integer');
		await expect(validateIntegerDivisionPrecision(-1, 1, 1))
			.rejects.toThrowError('Numerator must be a positive integer');
	});
	test('should fail if denominator is not a positive integer', async () =>
	{
		await expect(validateIntegerDivisionPrecision(1, 1.2, 1))
			.rejects.toThrowError('Denominator must be a positive integer');
		await expect(validateIntegerDivisionPrecision(1, 0, 1))
			.rejects.toThrowError('Denominator must be a positive integer');
		await expect(validateIntegerDivisionPrecision(1, -1, 1))
			.rejects.toThrowError('Denominator must be a positive integer');
	});
	test('should fail if division precision steps is not a positive integer', async () =>
	{
		await expect(validateIntegerDivisionPrecision(1, 1, 1.2))
			.rejects.toThrowError('Division precision steps must be a positive integer');
		await expect(validateIntegerDivisionPrecision(1, 1, 0))
			.rejects.toThrowError('Division precision steps must be a positive integer');
		await expect(validateIntegerDivisionPrecision(1, 1, -1))
			.rejects.toThrowError('Division precision steps must be a positive integer');
	});
	test('should fail if division precision is not sufficient', async () =>
	{
		const numerator = 100;
		const denominator = 10;
		const requiredSteps = 100;
		await expect(validateIntegerDivisionPrecision(numerator, denominator, requiredSteps))
			.rejects.toThrowError('The division (100 / 10) must have at most 1.00% error (at least 100 steps). Actual worst case precision is 10.00% (10.0 steps).');
	});
});
